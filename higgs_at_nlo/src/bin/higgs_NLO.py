#!/usr/bin/env python3
	
if __name__ == "__main__":
	
	try:
		import pycuba
	except:
		print("Error: pycuba cannot be imported correctly.")
		print("The instructions to install PyCuba can be found on the following link")
		print("https://johannesbuchner.github.io/PyMultiNest/install.html")
		print("For more informations you can consult the documentation in hjet_at_nlo/doc")
	import numpy as np
	import argparse


	import sys
	sys.path.insert(0,"../lib")
	try:
		from univ_const import mH, v2, ca, bg1, beta0, GeV_pb
		from boost_function import longi_boost
		from myHisto import myHisto
		from pdf import myPdf
	except:
		print("""Achtung: Insert manually in the header of the "higgs_NLO.py" code the PATH to the folder "lib" """)



	parser = argparse.ArgumentParser(description = 'set the value of the parameters' )

	parser.add_argument('--pdf_set','-pdf',dest = 'pdf_set',nargs='?',default = 'NNPDF31_nlo_as_0118',type = str, help = "default: 'NNPDF31_nlo_as_0118' ")
	parser.add_argument('--energy','-e',dest = 'sqrtS_hadronic', nargs = '?',default = 13000. ,type = float, help = "default: 13000.")
	parser.add_argument('--fac_scale','-muf',dest = 'fac_scale', nargs = '?', default = mH, type = float, help = "default: 125.1")
	parser.add_argument('--ren_scale','-mur',dest = 'ren_scale', nargs = '?', default = mH, type = float, help = "default: 125.1")
	parser.add_argument('--ptHiggs','-pth',dest = 'pth',nargs = '?', default = 0.5, type = float, help = "default: 0.1 GeV")

	parser.add_argument('--include_born','-born',dest='born_only',action='store_true',help = "born contribution only")
	parser.add_argument('--include_hfunc','-hfunc',dest='hfunc_only',action='store_true',help = "Hfunction contribution only (note: Born included)")
	parser.add_argument('--include_ct','-ct',dest='ct_only',action='store_true',help = "counterterm contribution only")
	#parser.add_argument('--all_pieces','-all',dest='all_pieces',action='store_true',help = "Hfunction - counterterm")
		
	parser.add_argument('--ep_rel',dest='ep_rel',nargs='?',default=1e-3,type = float,help= "relative precision of MC integration, default: 1e-3")
	parser.add_argument('--N_increase','-N_inc',dest='N_increase',nargs='?',default=10000,type = int,help= "increase number of points per iterations, type = int, default: 1000")
	parser.add_argument('--max_eval','-max',dest='max_eval',nargs='?',default=200000,type = int,help= "max number of MC evaluations, type =int, default: 10000")


	parser.add_argument('--writehisto','-histo',dest='writehist',action='store_true',help = "write .txt file of histo")
	parser.add_argument("--name_npz",'-npz',dest='namefile',nargs = '?',default = "histo",type=str,help = "default name of npz file is histo")

	args = parser.parse_args()

	#--- pdf set ---#
	pdfSet = args.pdf_set

	#---- kinematics ----#
	S = (args.sqrtS_hadronic)**2
	fac_scale = args.fac_scale
	ren_scale = args.ren_scale
	pt_h_min = args.pth

	#---- process ----#
	born_only = args.born_only
	hfunc_only = args.hfunc_only
	ct_only = args.ct_only
	#all_pieces = args.all_pieces

	#---- npz file ----#
	writehisto = args.writehist
	name_npz = args.namefile

	#--------- MC integration ---------#
	ep_rel = args.ep_rel
	N_increase = args.N_increase
	max_eval = args.max_eval

	#--- make instance of myPdf class ---#
	pdf_x1 = myPdf(pdfSet)
	pdf_x2 = myPdf(pdfSet)
	coupl = myPdf(pdfSet)
	pdf_x1_z1 = myPdf(pdfSet)
	pdf_x2_z2 = myPdf(pdfSet)






	print("")
	print("")
	print("#********************************************************************#")
	print("#                              _ _        _ _                        #")
	print("#                     |  |    |   | |    |   |                       #")
	print("#                     |--| -- |   | |    |   |                       #")
	print("#                     |  |    |   | |_ _ |_ _|                       #")
	print("#                                                                    #")
	print("#********************************************************************#")
	print("")
	print("#---------------------------- pdf set -----------------------------#")
	print("")
	print("%s"%(pdfSet))
	print("")
	print("#--------------------- kinematical variables ----------------------#")
	print("")
	print("Hadronic com energy sqrt(S) = %s GeV"%(np.sqrt(S))) 
	print("ptHiggs_min = %s GeV"%(pt_h_min))
	print("renScale = %s GeV, facScale = %s GeV"%(ren_scale,fac_scale))
	print("")
	print("#-------------------------- Processes ----------------------------#")
	print("")
	print("Born: %s"%(born_only))
	print("Hfunction: %s"%(hfunc_only))
	print("counterterm: %s"%(ct_only))
	print("")
	print("#------------------------- write histo ---------------------------#")
	print("")
	print("%s"%(writehisto))
	print("")
	print("#------------------------ MC parameters --------------------------#")
	print("")
	print("relative precision: %s"%(ep_rel))
	print("N increase: %s"%(N_increase))
	print("Max evaluations %s"%(max_eval))
	print("")
	print("#******************************************************************#")




	#-------- initialize empty list for quantities that could be plotted --------#
	rap_higgs = []
	



	# initialize empty list for weights given by Vegas integration
	wgt_vegas = []
	integrand_wgt = []
	iterations = []




	

	def Integrand(ndim, xx, ncomp, ff, userdata, nvec, core, weight, iter):  

		y,z1,z2 = [xx[i] for i in range(ndim.contents.value)]   # (tau,phi,xt2) var needed for real contribution only

		
		#---------------------- Sampling -----------------------#
		#--------- Kinematical Variables for gg -> H process ---------#
		xt2cut = pt_h_min**2 / mH**2

		tau_B = mH**2 / S    # minimal tau for g g -> H process
		y_B = 0.5 * np.log(tau_B) - np.log(tau_B) * y   # rap for gg -> H process

		# Importance sampling on z1 (z1 convolution integral)
		x1_B = np.sqrt(tau_B) * np.exp(y_B)
		lnz1 = z1 * np.log(x1_B)
		z1new = np.exp(lnz1)

		# Importance sampling on z2 (z2 convolution integral)
		x2_B = np.sqrt(tau_B) * np.exp(-y_B)
		lnz2 = z2 * np.log(x2_B)
		z2new = np.exp(lnz2)
		
		# Jacobians from sampling
		Jy_B = -2. * np.log(np.sqrt(tau_B))
		Jz1 = -np.log(x1_B) * z1new
		Jz2 = -np.log(x2_B) * z2new

		#********* evaluate pdfs and coupling ***********#
		pdf_x1.eval_pdf(fac_scale**2,x1_B)
		pdf_x2.eval_pdf(fac_scale**2,x2_B)
		pdf_x1_z1.eval_pdf(fac_scale**2,x1_B / z1new)
		pdf_x2_z2.eval_pdf(fac_scale**2,x2_B / z2new)
		coupl.eval_coupl(ren_scale)

		#------------------------------------#

		#-------- Born amplitude for gg -> H --------#
		born = coupl.alphas**2 / np.pi * mH**2 / (576. * v2 * S)

		
		# gluon pdf
		gluon_pdf = pdf_x1.glu * pdf_x2.glu

		#--- cumulants
		tot = 0.


		#************************* Born cross section *************************#

		
		born_xsec = GeV_pb * (Jy_B * gluon_pdf * born)    # born cross section gg -> H
		
		if born_only:
			tot += born_xsec

		#---------------------------------------------------------------------#
		#-------------------------- Begin HFunction --------------------------#
		#---------------------------------------------------------------------#
		if hfunc_only:

			#----------------------- MS-bar contribution -----------------------#
			
			# z1 convolution		
			conv_x1_B_1  = (ca * (pdf_x1_z1.glu / z1new - pdf_x1.glu) / (1. - z1new) + \
							ca * pdf_x1_z1.glu / z1new * ( (1. - 2. * z1new) / z1new + z1new * (1. - z1new)))
			
			conv_0_x1_B = ca * pdf_x1.glu * np.log(1. - x1_B)
			
			
			# z2 convolution
			conv_x2_B_1 = (ca * (pdf_x2_z2.glu / z2new - pdf_x2.glu) / (1. - z2new) + \
						ca * pdf_x2_z2.glu / z2new * ((1. - 2. * z2new) / z2new + z2new * (1. - z2new)))
			
			conv_0_x2_B = ca * pdf_x2.glu * np.log(1. - x2_B)
			
			#------------------------------------#
			L_facScale = np.log(fac_scale**2 / mH**2)

			
			msbar = - coupl.alphas / np.pi * ((pdf_x2.glu * (conv_0_x1_B + conv_x1_B_1 * Jz1 - bg1 / 2. * pdf_x1.glu)  + \
			  		pdf_x1.glu * (conv_0_x2_B + conv_x2_B_1 * Jz2 - bg1 / 2. * pdf_x2.glu)) ) * L_facScale

			
			#----------------------- Hard collinear coefficient -----------------------#
			H1g = coupl.alphas / np.pi * (ca * np.pi**2 / 2. + 11. / 2. )  # see eq.(85,86) of paper: 1311.1654 

			H1g -= coupl.alphas / np.pi * 2 * beta0 * np.log(mH**2 / ren_scale**2)  # Include log factor from running of coupling

			hfunc = born_xsec * H1g + GeV_pb * born * Jy_B * msbar   

			hfunc += born_xsec

			tot += hfunc
		#-------------------------------------------------------------------#
		#-------------------------- END HFunction --------------------------#
		#-------------------------------------------------------------------#


		#-----------------------------------------------------------------------#
		#-------------------------- Begin Counterterm --------------------------#
		#-----------------------------------------------------------------------#

		if ct_only:
			
			# z1 convolution		
			conv_x1_B_1  = (ca * (pdf_x1_z1.glu / z1new - pdf_x1.glu) / (1. - z1new) + \
							ca * pdf_x1_z1.glu / z1new * ( (1. - 2. * z1new) / z1new + z1new * (1. - z1new)))
			
			conv_0_x1_B = ca * pdf_x1.glu * np.log(1. - x1_B)
			
			
			# z2 convolution
			conv_x2_B_1 = (ca * (pdf_x2_z2.glu / z2new - pdf_x2.glu) / (1. - z2new) + \
						ca * pdf_x2_z2.glu / z2new * ((1. - 2. * z2new) / z2new + z2new * (1. - z2new)))
			
			conv_0_x2_B = ca * pdf_x2.glu * np.log(1. - x2_B)

			#--------- Integrand pieces ---------#

			Lcut = np.log(xt2cut)
			
			I1 = coupl.alphas / np.pi * born * Jy_B * (0.5 * ca * gluon_pdf * Lcut**2)
			
			I2 = - coupl.alphas / np.pi * born * Jy_B * (bg1 * gluon_pdf * Lcut)
			
			I3 = - coupl.alphas / np.pi * born * Jy_B * (pdf_x2.glu * (conv_0_x1_B + conv_x1_B_1 * Jz1 - bg1 / 2. * pdf_x1.glu)  + \
			  		pdf_x1.glu * (conv_0_x2_B + conv_x2_B_1 * Jz2 - bg1 / 2. * pdf_x2.glu)) * Lcut

			counterterm = GeV_pb * (I1 + I2 + I3)   # qT subtraction counterterm, see resummation eq. (64) of paper  hep-ph/0508068

		
			tot += counterterm
		#---------------------------------------------------------------------#
		#-------------------------- End Counterterm --------------------------#
		#---------------------------------------------------------------------#

		if writehisto:
			if iter[0] > 2 :  # the first two Vegas iterations are skipped
				iterations.append(iter[0])
				wgt_vegas.append(weight[0])
				integrand_wgt.append(tot)


				#--- 2 -> 1 Kin., p(P1) p(P2) -> H(p)

				#--- momenta in partonic frame ---#
				shat_B = x1_B * x2_B * S
				p1 = np.sqrt(shat_B) / 2. * np.array([1,0,0,1])
				p2 = np.sqrt(shat_B) / 2. * np.array([1,0,0,-1])
				
				pHiggs = p1 + p2

				#--- momenta in hadronic frame ---#
				refvec = np.sqrt(S) / 2. * np.array([x1_B + x2_B,0,0,x2_B - x1_B])

				p1had = longi_boost(p1,refvec)
				p2had = longi_boost(p2,refvec)
				pHiggshad = p1had + p2had
				

				#-- Observables --#

				yH  = 0.5 * np.log((pHiggshad[0] + pHiggshad[3]) / (pHiggshad[0] - pHiggshad[3]))
				
				
				rap_higgs.append(yH)

			



		ff[0] = tot
		return  0
	

	NDIM = 3
	NCOMP = 1
	EPSREL = ep_rel
	NINCREASE = N_increase
	MAXEVAL = max_eval

	def print_header(name):
		print('-------------------- %s test -------------------' % name)
	def print_results(name, results):
		keys = ['nregions', 'neval', 'fail']
		text = ["%s %d" % (k, results[k]) for k in keys if k in results]
		print("%s RESULt:\t" % name.upper() + "\t".join(text))
		for comp in results['results']:
			print("%s RESULT:\t" % name.upper() + \
				"%(integral).8f +- %(error).8f\tp = %(prob).3f\n" % comp)

	from os import environ as env
	verbose = 2
	if 'CUBAVERBOSE' in env:
		verbose = int(env['CUBAVERBOSE'])

	print_header('Vegas')
	print_results('Vegas', pycuba.Vegas(Integrand, NDIM,epsrel=EPSREL,maxeval=MAXEVAL,\
                    nincrease=NINCREASE, verbose=2))





if writehisto:

	wgts = np.array(wgt_vegas) * np.array(integrand_wgt) / (iterations[-1] - 2)

	rapHiggs = myHisto("Higgs rapidity")
	
	rapHiggs.fill(rap_higgs,-5.,5.,0.4,wgts / 0.4)
	
	np.savez(name_npz,histrap = rapHiggs.histo,binedges_rap = rapHiggs.binedges,error_rap = rapHiggs.error)
	


  







