#!/usr/bin/env python3


if __name__ == "__main__":

	import pycuba
	import numpy as np
	import argparse

	import sys
	sys.path.insert(0,"../lib")
	from univ_const import mH, v2, ca, GeV_pb
	from boost_function import *
	from myHisto import myHisto
	from pdf import myPdf

	parser = argparse.ArgumentParser(description = 'set the value of the parameters' )

	parser.add_argument('--pdf_set','-pdf',dest = 'pdf_set',nargs='?',default = 'NNPDF31_nlo_as_0118',type = str, help = "default: 'NNPDF31_nlo_as_0118' ")
	parser.add_argument('--energy','-e',dest = 'sqrtS_hadronic', nargs = '?',default = 13000. ,type = float, help = "default: 13000.")
	parser.add_argument('--fac_scale','-muf',dest = 'fac_scale', nargs = '?', default = mH, type = float, help = "default: 125.1")
	parser.add_argument('--ren_scale','-mur',dest = 'ren_scale', nargs = '?', default = mH, type = float, help = "default: 125.1")
	parser.add_argument('--ptHiggs','-pth',dest = 'pth',nargs = '?', default = 0.5, type = float, help = "default: 0.1 GeV")
		
	parser.add_argument('--ep_rel',dest='ep_rel',nargs='?',default=1e-3,type = float,help= "relative precision of MC integration, default: 1e-3")
	parser.add_argument('--N_increase','-N_inc',dest='N_increase',nargs='?',default=10000,type = int,help= "increase number of points per iterations, type = int, default: 1000")
	parser.add_argument('--max_eval','-max',dest='max_eval',nargs='?',default=200000,type = int,help= "max number of MC evaluations, type =int, default: 10000")

	parser.add_argument('--writehisto','-histo',dest='writehist',action='store_true',help = "write .txt file of histo")
	parser.add_argument("--name_npz",'-npz',dest='namefile',nargs = '?',default = "histo",type=str,help = "default name of npz file is histo")

	args = parser.parse_args()



	#--- pdf set ---#
	pdfSet = args.pdf_set

	#---- kinematics ----#
	S = (args.sqrtS_hadronic)**2
	fac_scale = args.fac_scale
	ren_scale = args.ren_scale
	pt_h_min = args.pth
	#pt_h = [0.08]#np.linspace(0.01,1.,100)

	#---- npz file ----#
	writehisto = args.writehist
	name_npz = args.namefile

	#--------- MC integration ---------#
	ep_rel = args.ep_rel
	N_increase = args.N_increase
	max_eval = args.max_eval

	#--- make instance of myPdf class ---#
	pdf_x1 = myPdf(pdfSet)
	pdf_x2 = myPdf(pdfSet)
	coupl = myPdf(pdfSet)





	print("")
	print("")
	print("#********************************************************************#")
	print("#                              _ _        _ _                        #")
	print("#                     |  |    |   | |    |   |                       #")
	print("#                     |--| -- |   | |    |   |                       #")
	print("#                     |  |    |   | |_ _ |_ _|                       #")
	print("#                                                                    #")
	print("#********************************************************************#")
	print("")
	print("#---------------------------- pdf set -----------------------------#")
	print("")
	print("%s"%(pdfSet))
	print("")
	print("#--------------------- kinematical variables ----------------------#")
	print("")
	print("Hadronic com energy sqrt(S) = %s GeV"%(np.sqrt(S))) 
	print("ptHiggs_min = %s GeV"%(pt_h_min))
	print("renScale = %s GeV, facScale = %s GeV"%(ren_scale,fac_scale))
	print("")
	print("#------------------------- write histo ---------------------------#")
	print("")
	print("%s"%(writehisto))
	print("")
	print("#------------------------ MC parameters --------------------------#")
	print("")
	print("relative precision: %s"%(ep_rel))
	print("N increase: %s"%(N_increase))
	print("Max evaluations %s"%(max_eval))
	print("")
	print("#******************************************************************#")


	#-------- initialize empty list for quantities that could be plotted --------#
	rap_higgs = []
	



	# initialize empty list for weights given by Vegas integration
	wgt_vegas = []
	integrand_wgt = []
	iterations = []

	

	def Integrand(ndim, xx, ncomp, ff, userdata, nvec, core, weight, iter):  

		y,tau,phi,xt2 = [xx[i] for i in range(ndim.contents.value)]   # (tau,phi,xt2) var needed for real contribution only

	
		#---------------------- Sampling -----------------------#

		#--------- Kinematical Variables for gg -> H g process ---------#
		tau_R = 2. * np.sqrt(( mH**2 * pt_h_min**2 + pt_h_min**4)/S**2) + (mH**2 + 2. * pt_h_min**2) / S # minimal tau for g g -> h g process
		
		#-- importance sampling on tau_R --#
		lntau = tau * np.log(tau_R)
		tau_R_new = np.exp(lntau)

		y_R = 0.5 * np.log(tau_R_new) - np.log(tau_R_new) * y   # rap for g g -> H g process
		phinew = 2. * np.pi * phi

		x1_R = np.sqrt(tau_R_new) * np.exp(y_R)
		x2_R = np.sqrt(tau_R_new) * np.exp(-y_R)

		Q2 = tau_R_new * S
		qt2max = (Q2 - mH**2)**2 / (4. * Q2)
		xt2max = qt2max / Q2
		xt2min = pt_h_min**2 / Q2
		
		# importance sampling on xt2
		logxt2 = np.log(xt2min) + (np.log(xt2max) - np.log(xt2min)) * xt2
		xt2new = np.exp(logxt2)


		#********* evaluate pdfs and coupling ***********#
		pdf_x1.eval_pdf(fac_scale**2,x1_R)
		pdf_x2.eval_pdf(fac_scale**2,x2_R)
		coupl.eval_coupl(ren_scale)


		#-- jacobian for g g -> h g
		HypCubeFac = (-np.log(tau_R) * tau_R_new) * (- np.log(tau_R_new) ) * (2. * np.pi) * (np.log(xt2max) - np.log(xt2min)) * xt2new

		# Phase space for g g -> h g process 
		PS = 1.  / (32. * np.pi**2 * (Q2)) * 1 / (np.sqrt(xt2max - xt2new))

		#------------------------------------#


		#----------------------------------------------------------------#
		#-------------------------- Begin Real --------------------------#
		#----------------------------------------------------------------#
		
		num_M2 = coupl.alphas**3 * ( 2. * xt2new**2 * Q2**4  + (mH**2 - Q2)**4  - 4. * xt2new * Q2**2 *\
					(mH**2 - Q2)**2 + mH**8  + Q2**4)

		den_M2 = 24. * np.pi * v2 * xt2new * Q2**3

		amp2 = num_M2 / den_M2

		gghg =  GeV_pb * HypCubeFac * pdf_x1.glu * pdf_x2.glu * amp2 * PS


		#--------------------------------------------------------------#
		#-------------------------- End Real --------------------------#
		#--------------------------------------------------------------#			

		if writehisto:
			if iter[0] > 2 :  # the first two Vegas iterations are skipped
				iterations.append(iter[0])
				wgt_vegas.append(weight[0])
				integrand_wgt.append(gghg)

				#--- 2 -> 2 Kin., p(P1) p(P2) -> j(p3) + H(p4)

				E3 = np.sqrt(Q2) / 2. * (1. - mH**2 / Q2)
				E4 = np.sqrt(Q2) / 2. * (1. + mH**2 / Q2)

				#--- momenta in partonic frame ---#

				p1 = np.sqrt(Q2) / 2. * np.array([1,0,0,1])
				p2 = np.sqrt(Q2) / 2. * np.array([1,0,0,-1])
				b = 1.
				if np.random.rand() < 0.5:
					b = -1.

				#--- from qt2 to t
				a = 1.

				if (np.random.rand() < 0.5):
					a = -1.

				t = - np.sqrt(Q2 * qt2max) * ( 1. - a * np.sqrt(1 - Q2 * xt2new / qt2max ) )

				u = - Q2 - t + mH**2

				p3 = E3 * np.array([1,  b * np.sqrt(- 2. * t / (np.sqrt(Q2) * E3) * (1. + 0.5 * t / (np.sqrt(Q2) * E3))) * np.cos(phinew), \
					b * np.sqrt(- 2. * t / (np.sqrt(Q2) * E3) * (1. + 0.5 * t / (np.sqrt(Q2) * E3))) * np.sin(phinew) , \
					1. + t / (np.sqrt(Q2) * E3)])

				p4 = p1 + p2 - p3

				#--- momenta in hadronic frame ---#
				refvec = np.sqrt(S) / 2. * np.array([x1_R + x2_R,0,0,x2_R - x1_R])

				p1had = longi_boost(p1,refvec)
				p2had = longi_boost(p2,refvec)
				p3had = longi_boost(p3,refvec)
				p4had = p1had + p2had - p3had

				#-- Observables --#

				yH  = 0.5 * np.log((p4had[0] + p4had[3]) / (p4had[0] - p4had[3]))

				#--- 2 -> 1 Kin., p(P1) p(P2) -> H(p)

				#--- momenta in partonic frame ---#
				# shat_R = x1_R * x2_R * S
				# p1 = np.sqrt(shat_R) / 2. * np.array([1,0,0,1])
				# p2 = np.sqrt(shat_R) / 2. * np.array([1,0,0,-1])
				
				# pHiggs = p1 + p2

				# #--- momenta in hadronic frame ---#
				# refvec = np.sqrt(S) / 2. * np.array([x1_R + x2_R,0,0,x2_R - x1_R])

				# p1had = longi_boost(p1,refvec)
				# p2had = longi_boost(p2,refvec)
				# pHiggshad = p1had + p2had
				

				# #-- Observables --#

				# yH  = 0.5 * np.log((pHiggshad[0] + pHiggshad[3]) / (pHiggshad[0] - pHiggshad[3]))
				
				
				rap_higgs.append(yH)
				

		ff[0] = gghg
		return  0
	

	NDIM = 4  
	NCOMP = 1
	EPSREL = ep_rel
	NINCREASE = N_increase
	MAXEVAL = max_eval

	def print_header(name):
		print('-------------------- %s test -------------------' % name)
	def print_results(name, results):
		keys = ['nregions', 'neval', 'fail']
		text = ["%s %d" % (k, results[k]) for k in keys if k in results]
		print("%s RESULt:\t" % name.upper() + "\t".join(text))
		for comp in results['results']:
			print("%s RESULT:\t" % name.upper() + \
				"%(integral).8f +- %(error).8f\tp = %(prob).3f\n" % comp)

	from os import environ as env
	verbose = 2
	if 'CUBAVERBOSE' in env:
		verbose = int(env['CUBAVERBOSE'])

	print_header('Vegas')
	print_results('Vegas', pycuba.Vegas(Integrand, NDIM,epsrel=EPSREL,maxeval=MAXEVAL,\
                    nincrease=NINCREASE, verbose=2))

	



if writehisto:

	wgts = np.array(wgt_vegas) * np.array(integrand_wgt) / (iterations[-1] - 2)

	rapHiggs = myHisto("Higgs rapidity")

	rapHiggs.fill(rap_higgs,-5.,5.,0.4,wgts / 0.4)

	np.savez(name_npz,histrap = rapHiggs.histo,binedges_rap = rapHiggs.binedges,error_rap = rapHiggs.error)


			
	


  







