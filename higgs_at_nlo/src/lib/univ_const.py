#!/usr/bin/env python3
import numpy as np

#-------------- Fixed Data --------------#
mH = 125.1    #GeV Higgs Mass

v2 = (246.250837183427)**2  #vev GeV**2

GeV_pb = 0.389379*10**9  # Conversion factor from GeV to pb

ca = 3.   #quadratic casimir invariant of the adjoint

nf = 5.  # number of active light flavors in HEFT

bg1 = -(33.-2.*nf)/6.  # resummation coefficient

beta0 = (11. * ca - 2. / 3. * nf * ca ) / 12.   # beta0 in the qt-subtraction convention

EulerGamma = 0.5772156649015329    # euler mascheroni Constant

#---------------------------------------#

