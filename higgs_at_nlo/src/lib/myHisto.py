import numpy as np 



class myHisto:

	""" class myHisto: when instantiated creates an object with name
		methods: fill(self, samples, bins)
		e.g. 
		x = myHisto("histo")
		x.fill(np.random.rand(100),np.linspace(0,1,20),weights)
		x.histo # --> histogrammed values
		x.binedges # --> binedges	     """

	def __init__(self,name):
		self.name = name

	def fill(self,samples,minbin,maxbin,dx,wgts):

		Nbins = int((maxbin - minbin) / dx) + 1
		linbins = np.linspace(minbin,maxbin,Nbins)
		self.histo = np.histogram(samples,linbins,weights = wgts)[0]
		self.binedges = np.histogram(samples,linbins, weights = wgts)[1]

		sum_w2 = np.zeros([len(linbins)-1], dtype=np.float32)  ## start with empty errors
		digits = np.digitize(samples, linbins)                 ## bin index array for each data element

		for i in range(len(linbins)-1):
			weights_in_current_bin = wgts[np.where(digits == i)[0]]
			sum_w2[i] = np.sum(np.power(weights_in_current_bin, 2))
			

		self.error = np.sqrt(sum_w2)





