# H Jet NLO
Repository for the codes higgs_at_nlo and hjet_at_nlo.

## Description
hjet at nlo computes the Higgs plus jet NLO cross section in proton-proton collisions. In particular the qT - subtraction method is used to remove the infrared divergences for each subprocess.

## documentation
Documentation regarding the code hjet_at_nlo can be found in hjet_at_nlo/doc
