\documentclass{article}
\usepackage{latexsym}
\usepackage{amssymb}
\usepackage{amsmath}

\textwidth=6.25in
\textheight=10.1in
\voffset=-1in
\hoffset=-0.5in
\oddsidemargin=0.5in




%%%%%%%%%% personalized commands %%%%%%%%%%
\newcommand{\quotes}[1]{``#1''}





%----
\usepackage{slashed}

% Stuff for box
\usepackage{tcolorbox}
\tcbuselibrary{theorems} % Def/Theorem Box

\newtcolorbox{mybox}[1]{breakable, sharp corners, colback=white!5!white,colframe=white!75!black,fonttitle=\bfseries,title=#1}
%-------------------------------

\title{Hjet at NLO}

\author{Mark Nestor Costantini }
\date{May 2021}

\begin{document}

\maketitle

\begin{center}
\vspace{12mm}
\begin{quote}
\pretolerance 10000
	This is a note about the numerical program {\tt hjet$\_$at$\_$nlo}. {\tt hjet$\_$at$\_$nlo}
	computes the Higgs plus jet NLO cross section in $pp$ collisions. In particular the $q_T$ - subtraction method is used
	to remove the infrared divergences for each subprocess.
\end{quote}
\end{center}

\section{dependencies}
To run the code it is necessary that you installed {\tt python} in particular you will need the {\tt NumPy} and {\tt SciPy} packages. Both of these can be installed from command line using the Python package installer pip in the following manner\\
{\tt pythonX.Y -m pip install numpy}\\
{\tt pythonX.Y -m pip install scipy}\\
\subsection{{\tt PyCuba}}
The Monte Carlo numerical integrations are performed by the {\tt PyCuba} library, a {\tt python} interface for the {\tt Cuba} library for multidimensional numerical integration. The instructions to install {\tt PyCuba} can be found on the following link\\
{\tt https://johannesbuchner.github.io/PyMultiNest/install.html}\\
In particular to run the code you will need to add four more arguments to the {\tt integrand$\_$type} function that can be found in the wrapper.\\
This can be done as follows:\\
Open the file {\tt $\_\_$init$\_\_$.py} in {\tt PyMultiNest/pycuba}\\
now add to the function {\tt integrand$\_$type} the four following pointers in the listed order:\\
{\tt POINTER(c$\_$int)}, {\tt POINTER(c$\_$int)}, {\tt POINTER(c$\_$double)}, {\tt POINTER(c$\_$int)}

\subsection{{\tt LHAPDF}}
{\tt hjet$\_$at$\_$nlo} works with the python interface of the {\tt LHAPDF} library.
If {\tt LHAPDF} is not already installed on your computer you can download the LHAPDF tar at the following link:\\
{\tt https://lhapdf.hepforge.org/downloads/}\\
In the {\tt LHAPDF-X.Y.Z} folder run in the following order:\\
{\tt ./configure --prefix=FOLDER$\_$PATH")}\\
{\tt make}\\
{\tt make install}\\
Add then the {\tt FOLDER$\_$PATH} to your PATH.\\
To use the {\tt python} interface you should add to your PYTHONPATH:\\
{\tt PYTHONPATH=$\$$PYTHONPATH:/FOLDER$\_$PATH/lib/pythonX.Y/site-packages}\\
Check if this works by trying to import lhapdf in a {\tt python} file.\\
The pdf set can then be downloaded from :\\
{\tt https://lhapdf.hepforge.org/pdfsets.html")}\\
unpack and add the pdf set folder under:\\
{\tt /FOLDER$\_$PATH/share/LHAPDF}


\section{H-function}
The code to compute the NLO H-function for Higgs plus jet can be found in\\
{\tt hjet$\_$at$\_$nlo/src/bin/NLO$\_$HFunction$\_$HJet.py}\\
The usage of this function is rather easy.\\
By calling the {\tt python} module in the following manner:\\
{\tt python NLO$\_$HFunction$\_$HJet.py --help}\\
a message is printed to the console listing all the possible flags needed for the correct usage of the function. In particular, the most important commands are:\\
\begin{itemize}
	\item {\tt python NLO$\_$HFunction$\_$HJet.py -e X} $\to$ set the hadronic center of mass enegy to X TeV(default: 13 TeV).
	\item {\tt python NLO$\_$HFunction$\_$HJet.py -pt X} $\to$ set the minimal transverse momentum of the jet to X GeV(default: 30 GeV).
	\item {\tt python NLO$\_$HFunction$\_$HJet.py -rad X} $\to$ set the jet radius to $R=$X (default: $R=0.1$).
	\item {\tt python NLO$\_$HFunction$\_$HJet.py --pdf X} $\to$ set the PDF set used by {\tt LHAPDF} to X (default: {\tt NNPDF31$\_$nlo$\_$as$\_$0118}).
\end{itemize}
Moreover, by setting the flags {\tt --no-gg}, {\tt --no-qg} and {\tt --no-qq} it is possible to exclude this channels. Note that by default all channels are included.

\section{Counterterm}
The counterterm function can be found in\\
{\tt hjet$\_$at$\_$nlo/src/bin/NLO$\_$counterterm$\_$HJet.py}\\
The usage is basically identical as for the H-function, the main difference being that the counterterm depends also on the $r_{\text{cut}}$ slicing parameter of $q_T$ subtraction. In particular, this value can be varied from command line in the following manner\\
\begin{align}
	{\tt python \;\; NLO\_counterterm\_HJet.py \; \text{{\tt --rcut} } \; X} \to \text{set $r_{\text{cut}}$ to X(default: 0.01).}
\end{align}
The user can only choose between the jet radii $R = 0.1,0.3$ and $0.8$ since the numerical values of the mismatch and soft decoupled integrals were calculated by default only for these jet radius values (and stored in {\tt hjet$\_$at$\_$nlo/src/lib/mismatch$\_$soft.txt}). This can however be easily changed by editing the code in {\tt hjet$\_$at$\_$nlo/src/lib/mismatch$\_$softdecoupling$\_$integrals.py}.
The correct way for changing the jet radius is to simultaneously change the value of the jet radius and the corresponding mismatch/soft integral. This is done in the following way
\begin{itemize}
	\item {\tt python NLO$\_$counterterm$\_$HJet.py -rad 0.1 --mm$\_$and$\_$soft 0} $\to$ is the default setting.
	\item {\tt python NLO$\_$counterterm$\_$HJet.py -rad 0.3 --mm$\_$and$\_$soft 1} $\to$ set jet radius to 0.3.
	\item {\tt python NLO$\_$counterterm$\_$HJet.py -rad 0.8 --mm$\_$and$\_$soft 2} $\to$ set jet radius to 0.8.
\end{itemize}








%---- Bibliography ----%
% \newpage
% \bibliographystyle{unsrt}
% \bibliography{biblio}{}

\end{document}
