# values of the mismatch and soft integrals for different jet radii
#  mismacth correspnds to Imm eq. 155, soft: Isoft in eq 163 of Jurgs notes
# R, mismatch, soft
0.1 	 0.005000002603985986 	  4.607670189084448 
0.3 	 0.045001898760090975 	  2.430446558756281 
0.8 	 0.3206886409653617 	  0.6066314233804293 
