#!/usr/bin/env python

try:
    import lhapdf
    lesgo = True

except:
    print("")
    print("############################ Begin ACHTUNG ############################")
    print("")
    print("Something went wrong in importing lhapdf in python")
    print("Note: lhapdf is imported in /src/lib/pdf.py ")
    print("")
    print("###################### LHAPDF, python interface ######################")
    print("hjet_at_nlo works with the python interface of the LHAPDF library")
    print("You can download the LHAPDF tar at the following link:")
    print("https://lhapdf.hepforge.org/downloads/")
    print("In the LHAPDF-X.Y.Z folder run in the following order:")
    print("./configure --prefix=FOLDER_PATH")
    print("make")
    print("make install")
    print("Add then the FOLDER_PATH to your PATH")
    print("To use the python interface you should add to your python PATH")
    print("PYTHONPATH=$PYTHONPATH:/FOLDER_PATH/lib/pythonX.Y/site-packages")
    print("Check if this works by trying to import lhapdf")
    print("")
    print("############################## PDF sets ###############################")
    print("download your favorite pdf set from:")
    print("https://lhapdf.hepforge.org/pdfsets.html")
    print("unpack and add the pdf set folder under:")
    print("/FOLDER_PATH/share/LHAPDF")
    print("")
    print("############################# End ACHTUNG #############################")
    lesgo = False

if lesgo:
    class myPdf:

        glu = 0.
        up = 0.
        aup = 0.
        d = 0.
        ad = 0.
        st = 0.
        ast = 0.
        c = 0.
        ac = 0.
        b = 0.
        ab= 0.

        alpha = 0.

        def __init__(self,pdf_set):
            self.p = lhapdf.mkPDF(pdf_set, 0) 
            
        def eval_pdf(self,Q2,x):
            
            self.glu = self.p.xfxQ2(21,x,Q2)/x
            self.up = self.p.xfxQ2(2,x,Q2)/x
            self.aup = self.p.xfxQ2(-2,x,Q2)/x
            self.d = self.p.xfxQ2(1,x,Q2)/x
            self.ad = self.p.xfxQ2(-1,x,Q2)/x
            self.st = self.p.xfxQ2(3,x,Q2)/x
            self.ast = self.p.xfxQ2(-3,x,Q2)/x
            self.c = self.p.xfxQ2(4,x,Q2)/x
            self.ac = self.p.xfxQ2(-4,x,Q2)/x
            self.b = self.p.xfxQ2(5,x,Q2)/x
            self.ab = self.p.xfxQ2(-5,x,Q2)/x

        def eval_coupl(self,Q):
            self.alphas = self.p.alphasQ(Q)


    
