#!/usr/bin/env python
import numpy as np
from scipy import special
import sys
from constants import mH, v2, GeV_pb, ca, cf,Pi, nf


#*******************************************************************************************#
#                                                                                           #
#      GENERAL DESCRIPTION:                                                                 #
#      Hard factors entering the q_t = 0 part of the q_t subtraction formula.               #
#      The hard factor includes a finite piece form the virtual contribution,               #
#      a finite piece from the jet function and a finite piece coming from                  #
#      the O(ep) expansion of the soft counterterm.                                         #
#      All these pieces can be found in the mathematica-notebooks folder                    #
#                                                                                           #
#                                                                                           #
#*******************************************************************************************#



def dilog(x):
  return special.spence(1. - x) 


def log(x): 
  return np.log(x)

def hard_factor_qg(R,muR,Q2,t,u):
  

  s = Q2

  qt2max=(s - mH**2)**2 / (4. * s)

  mandelt = t
  mandelu = u
  
  N = ca

  vev = np.sqrt(v2)

  musq = muR**2



  H1qg = (-23*log(s/musq))/4.+(178.44964107683398-(5.*mandelt*mandelu)/(\
          mandelt**2+s**2)-(5.*mandelu*s)/(mandelt**2+s**2)+8*dilog(mandelt/\
          mH**2)+18.*dilog(mandelu/mH**2)+8*dilog(1-mH**2/s)+8*log(1-mandelt/\
          mH**2)*log(-(mandelt/mH**2))-4.5*log(-(mandelt/mH**2))**2-11.*log(-(\
          mandelu/mH**2))+18*log(1-mandelu/mH**2)*log(-(mandelu/mH**2))-9*log(-(\
          mandelt/mH**2))*log(-(mandelu/mH**2))+0.5*log(-(mandelu/mH**2))**2+6*\
          log(R**(-2))+11.*log(s/mH**2)+10*log(-(mandelt/mH**2))*log(s/mH**2)-\
          10*log(-(mandelu/mH**2))*log(s/mH**2)+6*log(s**2/(mandelt*mandelu))+4*\
          log(R**(-2))*log(s**2/(mandelt*mandelu))+2*log(s**2/(mandelt*mandelu))\
          **2)/6.



  return  H1qg



def hard_factor_qq(R,muR,Q2,t,u):
  
  s = Q2

  qt2max=(s - mH**2)**2 / (4. * s)
  
  N = ca

  vev = np.sqrt(v2)

  musq = muR**2

  
  H1qq = (-23*log(s/musq))/4.+(-16.420864549310586-(10.*s*(t+u))/(t**2+u**2)-\
          23*log(R**2)+23*log(s**2/(t*u))-18*log(R**2)*log(s**2/(t*u))+9*log(\
          s**2/(t*u))**2-2.*(-4.+dilog(t/mH**2)+dilog(u/mH**2)+log(-(t/mH**2))*(\
          1.*log(1-t/mH**2)-1.*log(-(u/mH**2)))+log(-(u/mH**2))*log(1-u/mH**2))+\
          18*(4.444444444444445+2.*dilog(1-mH**2/s)+dilog(t/mH**2)+dilog(u/\
          mH**2)+log(-(t/mH**2))*(-0.5*log(-(t/mH**2))+log(1-t/mH**2))+log(-(u/\
          mH**2))*(-0.5*log(-(u/mH**2))+log(1-u/mH**2))))/12.


  return  H1qq


def hard_factor_gg(R,muR,Q2,t,u):
    
    s = Q2
    
    qt2max=(s - mH**2)**2 / (4. * s)

    a = 1.
    
    N = ca

    vev = np.sqrt(v2)

    musq = muR**2

    pt2 = t * u / Q2
    
    H1gg= (66-(ca*(-402+23*nf+36*Pi**2))/9.+(2*mH**2*(-5+N)*(s*t*u+mH**2*(t*u+\
                    s*(t+u))))/(mH**8+s**4+t**4+u**4)-(11*N-2*nf)*log(R**2)+3*(-33+2*nf)*\
                    log(s/musq)+log(s/pt2)*(11*N-2*nf-6*ca*log(R**2)+3*ca*log(s/pt2))+12.*\
                    N*(6.579736267392906+1.*dilog(1-mH**2/s)+1.*dilog(t/mH**2)+1.*dilog(u/\
                    mH**2)-0.25*log(-(t/mH**2))**2+1.*log(-(t/mH**2))*log(1-t/mH**2)-0.5*\
                    log(-(t/mH**2))*log(-(u/mH**2))-0.25*log(-(u/mH**2))**2+1.*log(-(u/\
                    mH**2))*log(1-u/mH**2)))/12.
    
    return H1gg



