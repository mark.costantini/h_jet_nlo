#!/usr/bin/env python3
import numpy as np

# input: reference four vector q in the hadronic center of mass frame, four vector p in the partonic com frame
# to be boosted in the lab frame in which q is given
# output: longitudinal boost of the vector p

def longi_boost(p,q):

	e = q[0]
	p3 = q[3]

	y = 0.5*np.log((e+p3)/(e-p3))

	phad = np.array([p[0]*np.cosh(y)+p[3]*np.sinh(y),p[1],p[2],p[0]*np.sinh(y)+p[3]*np.cosh(y)])

	return phad
