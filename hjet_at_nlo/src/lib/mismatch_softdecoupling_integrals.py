#!/usr/bin/env python
import numpy as np
from scipy.integrate import quad

#*********************************************************#
#		
#		GENERAL DESCRIPTION:
#		Mismatch and Soft (both Born decoupled)
#		integrals, see Jurgs notes for more details
#
#		NOTES:
#		when runned as __main__ produces an output file
#		"mismatch_soft.txt", that is read from the
#		NLO_counterterm_HJet.py code.
#
#*********************************************************#



def xi_min(x,R):
	return 1. + (x - (1. - R**2 / 2.)) + np.sqrt((x - (1. - R**2 / 2.))*(x + (1. + R**2 / 2.)))


def mismatch(x,R):
	return 2. / np.pi * np.log(xi_min(x,R)) / np.sqrt(1. - x**2)


def integrated_mismatch(Radius):
	
	I = quad(mismatch, 1. - Radius**2 / 2., 1. , args = (Radius) )
	I_mm = I[0]
	I_mm_err = I[1]
	
	return  I_mm , I_mm_err


def soft_decoupled(x,R):

	Isoft1 = - 1. / np.pi * np.log(1. - x**2) / np.sqrt(1. - x**2)

	Isoft2 = 2. / np.pi * 2 * x / (1. - x**2) * np.arctan( np.sqrt(1. - x**2) / (xi_min(x,R) - x) )

	return Isoft1 + Isoft2


def integrated_soft_decoupled(Radius):

	I = quad(soft_decoupled,1. - Radius**2 / 2., 1., args = (Radius) )
	I_s = I[0] - 2. / np.pi * np.log(Radius**2 * (1. - Radius**2 / 4.)) * \
                  np.arctan(2. * np.sqrt(1. - Radius**2 / 4.) / Radius) -  2. * np.log(2.)
	
	I_s_err = I[1]

	return I_s, I_s_err



def decoupled_soft_leading(Radius):
	
	return np.log( 1. / Radius**2 )



if __name__ == '__main__':

	R_jet = np.array([0.1,0.3,0.8])
	
	mismatch = [integrated_mismatch(R)[0] for R in R_jet]

	soft = [integrated_soft_decoupled(R)[0] for R in R_jet]

	with open("mismatch_soft.txt","w+") as data:
		data.write("# values of the mismatch and soft integrals for different jet radii")
		data.write("\n")
		data.write("#  mismacth correspnds to Imm eq. 155, soft: Isoft in eq 163 of Jurgs notes")
		data.write("\n")
		data.write("# R, mismatch, soft")
		data.write("\n")
		for i in range(len(R_jet)):
			data.write("%s \t %s \t  %s \n"%(R_jet[i],mismatch[i],soft[i]))

	with open("mismatch_soft_leading.txt","w+") as data:
		data.write("# values of the mismatch and soft integrals for different jet radii, leading R approx")
		data.write("\n")
		data.write("# R, mismatch, soft")
		data.write("\n")
		for i in range(len(R_jet)):
			data.write("%s \t %s \t  %s \n"%(R_jet[i],0.,decoupled_soft_leading(R_jet[i])))



