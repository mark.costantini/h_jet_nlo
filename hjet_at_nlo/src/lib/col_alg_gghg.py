from constants import ca

#------ Colour Factors for g g -> g g H ------# 
T12_gg = -ca/2
T13_gg = -ca/2
T23_gg = -ca/2
T2_gg = ca   # Colour Factor squared