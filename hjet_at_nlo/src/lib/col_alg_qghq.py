from constants import ca, cf

#--- Color algebra for born q g > h q ---#
T23_qg = - ca / 2.
T13_qg = (ca - 2. * cf) / 2.
T12_qg = - ca / 2. 
T1_sq_qg = cf
T3_sq_qg = cf
T2_sq_qg = ca
#----------------------------------------#