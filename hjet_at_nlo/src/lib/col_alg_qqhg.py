from constants import ca, cf

#---- Color algebra for qq > h g born ----#
T23_qq = - ca / 2.
T13_qq = - ca / 2.
T12_qq = (ca - 2. * cf) / 2. 
T1_sq_qq = cf
T2_sq_qq = cf
T3_sq_qq = ca
#-----------------------------------------#