#!/usr/bin/env python3

#Input: p1,p2 Four vectors
#Output: Scalar Product with Minkowski metric
def MinkDot(p1,p2):

	return p1[0]*p2[0]-p1[1]*p2[1]-p1[2]*p2[2]-p1[3]*p2[3]