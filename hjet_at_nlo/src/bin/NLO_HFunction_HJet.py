#!/usr/bin/env python
try:
	import pycuba
	lesgo = True
except:
	print("Error: pycuba cannot be imported correctly.")
	print("The instructions to install PyCuba can be found on the following link")
	print("https://johannesbuchner.github.io/PyMultiNest/install.html")
	print("For more informations you can consult the documentation in hj_nlo/doc")
	lesgo = False


if lesgo:
	if __name__ == '__main__':
		
		import argparse
		import numpy as np
		import sys
		sys.path.insert(0,'../lib/')
		from constants import mH, v2, GeV_pb, ca, cf, ag1, bg1, tr, nf
		from boost_function import longi_boost
		from pdf import myPdf
		from NLO_HardFactors_HJet import hard_factor_qg, hard_factor_qq, hard_factor_gg
		from myHisto import myHisto
		#--- Set Kinematics ---#

		parser = argparse.ArgumentParser(description = 'set the value of the parameters' )

		parser.add_argument('--no-gg',dest='gg_ch',action='store_false')
		parser.add_argument('--no-qg',dest='qg_ch',action='store_false')
		parser.add_argument('--no-qq',dest='qq_ch',action='store_false')

		parser.add_argument('--pdf','-p',dest = 'pdf_set',nargs='?',default = 'NNPDF31_nlo_as_0118',type = str, help = "default: 'NNPDF31_nlo_as_0118' ")
		parser.add_argument('--energy','-e',dest = 'sqrtS_hadronic', nargs = '?',default = 13000. ,type = float,help = "default: 13000.")
		parser.add_argument('--pt_min_jet','-pt',dest = 'pt_min_jet', nargs = '?', default = 30., type = float,help = "default: 30.")
		parser.add_argument('--jet_radius','-rad',dest = 'jet_radius', nargs = '?', default = 0.1, type = float,help = "default: 0.1")
		parser.add_argument('--fac_scale','-muf',dest = 'fac_scale', nargs = '?', default = mH, type = float,help = "default: 125.1")
		parser.add_argument('--ren_scale','-mur',dest = 'ren_scale', nargs = '?', default = mH, type = float,help = "default: 125.1")
		parser.add_argument('--ep_rel',dest='ep_rel',nargs='?',default=1e-3,type = float,help= "relative precision of MC integration, default: 1e-3")
		parser.add_argument('--N_increase','-N_inc',dest='N_increase',nargs='?',default=1000,type = int,help= "increase number of points per iterations, type = int, default: 1000")
		parser.add_argument('--max_eval','-max',dest='max_eval',nargs='?',default=10000,type = int,help= "max number of MC evaluations, type =int, default: 10000")


		parser.add_argument('--write_histo',dest='write_hist',action='store_true')
		parser.add_argument("--name_npz",'-npz',dest='name_hist_file',nargs = '?',default = "histo_hjet_Hf",type=str,help = "default name of npz file is histo_hjet_Hf")
		

		args = parser.parse_args()

		#--- pdf set ---#
		pdfSet = args.pdf_set
		#--- kinematics ---#
		S = (args.sqrtS_hadronic)**2
		qt_min_jet = args.pt_min_jet
		R_jet = args.jet_radius
		facScale = args.fac_scale
		renScale = args.ren_scale
		tau0 = 2. * np.sqrt(( mH**2 * qt_min_jet**2 + qt_min_jet**4)/S**2) + ( mH**2 + 2. * qt_min_jet**2 )/S # Minimal Value of tau 
		#--------- channels ----------#
		gg_channel = args.gg_ch
		qg_channel = args.qg_ch
		qq_channel = args.qq_ch
		#--------- MC integration ---------#
		ep_rel = args.ep_rel
		N_increase = args.N_increase
		max_eval = args.max_eval
		#------ write output ------#
		writehisto = args.write_hist
		#------ output file for histo -------#
		name_npz = args.name_hist_file

		pdf_x1 = myPdf(pdfSet)
		pdf_x2 = myPdf(pdfSet)
		coupl = myPdf(pdfSet)
		pdf_x1_z1 = myPdf(pdfSet)
		pdf_x2_z2 = myPdf(pdfSet)
		

		print("")
		print("")
		print("#********************************************************************#")
		print("#                                  _ _        _ _                    #")
		print("#                     |  |   |    |   | |    |   |                   #")
		print("#                     |--|   | -- |   | |    |   |                   #")
		print("#                     |  | __|    |   | |_ _ |_ _|                   #")
		print("#                                                                    #")
		print("#********************************************************************#")
		print("")
		print("")
		print("#********************************************************************#")
		print("#                                                                    #")
		print("#                p p -> H + Jet @ NLO, Hfunction                     #")
		print("#                         qT  Subtraction                            #")
		print("#																	#")
		print("#        all channels, i.e. qq', qg and gg are included              #")
		print("#               in the Integrand function                            #")
		print("#                                                                    #")
		print("#********************************************************************#")
		print("")
		print("#-0--------------------------- pdf set ------------------------------#")
		print("")
		print("%s"%(pdfSet))
		print("")
		print("#---------------------- kinematical variables -----------------------#")
		print("")
		print("Hadronic com energy sqrt(S) = %s GeV"%(np.sqrt(S))) 
		print("ptjet_min = %s GeV"%(qt_min_jet))
		print("jetRadius = %s"%(R_jet))
		print("renScale = %s GeV, facScale = %s GeV"%(renScale,facScale))
		print("")
		print("#------------------------ channels included -------------------------#")
		print("")
		print("gg channel: %s"%(gg_channel))
		print("qg channel: %s"%(qg_channel))
		print("qq channel: %s"%(qq_channel))
		print("")
		print("#--------------------------- write txt -----------------------------#")
		print("")
		print("%s"%(writehisto))
		if writehisto:
			print("histo saved to file: %s.npz"%(name_npz))
		print("")
		print("#------------------------- MC parameters ---------------------------#")
		print("")
		print("relative precision: %s"%(ep_rel))
		print("N increase: %s"%(N_increase))
		print("Max evaluations %s"%(max_eval))
		print("")
		print("#*******************************************************************#")

		

		#--------- Initialize empty list for all quantities that could be plotted ---------#

		rap_higgs = []
		pt_Higgs = []
		pt_jet = []
		pt_higgs_jet = []
		m_higgs_jet = []

		# initialize empty list for weights given by Vegas integration

		wgt_vegas = []
		integrand_wgt = []
		iterations = []


		
		def Integrand(ndim, xx, ncomp, ff, userdata, nvec, core, weight, iter):
		
			tau,y,phi,xt2,z1,z2 = [xx[i] for i in range(ndim.contents.value)]
			
			#******************************************************************************************#
			#*********************** sampling on Born and convolution variables ***********************#
			#******************************************************************************************#
			# Kinematical Variables
			# importance sampling on tau
			lntau = tau * np.log(tau0)
			taunew = np.exp(lntau)
			
			ynew = 0.5 * np.log(taunew) - np.log(taunew) * y
			phinew = 2. * np.pi * phi
			x1 = np.sqrt(taunew) * np.exp(ynew)
			x2 = np.sqrt(taunew) * np.exp(-ynew)

			# Importance sampling on z1 (z1 convolution integral)    
			lnz1 = z1 * np.log(x1)
			z1new = np.exp(lnz1)

			# Importance sampling on z2 (z2 convolution integral)
			lnz2 = z2 * np.log(x2)
			z2new = np.exp(lnz2)

			Q2 = taunew * S
			qt2max = (Q2 - mH**2)**2 / (4. * Q2)
			xt2max = qt2max / Q2
			xt2min = qt_min_jet**2 / Q2
			# importance sampling on xt2
			logxt2 = np.log(xt2min) + (np.log(xt2max) - np.log(xt2min)) * xt2
			xt2new = np.exp(logxt2)

			#********* evaluate all pdfs ***********#
			pdf_x1.eval_pdf(facScale**2,x1)
			pdf_x2.eval_pdf(facScale**2,x2)
			pdf_x1_z1.eval_pdf(facScale**2,x1 / z1new)
			pdf_x2_z2.eval_pdf(facScale**2,x2 / z2new)
			coupl.eval_coupl(renScale)

			#******************************************************************************************#
			#********************************** Born dependent stuff **********************************#
			#******************************************************************************************#

			#---- jacobian coming from the change of variable. (Cuba integration performed over Hypercube) ----#
			HypCubeFac = (-np.log(tau0) * taunew) * (- np.log(taunew) ) * (2. * np.pi) * (np.log(xt2max) - np.log(xt2min)) * xt2new
			
			#--- from t to qt2
			a = 1.
			
			if (np.random.rand() < 0.5):
					a = -1.

			t = - np.sqrt(Q2 * qt2max) * ( 1. - a * np.sqrt(1 - Q2 * xt2new / qt2max ) )

			u = - Q2 - t + mH**2

			# Phase Space factor, already includes flux factor

			PS = 1.  / (32. * np.pi**2 * (Q2)) * 1 / (np.sqrt(xt2max - xt2new))

			#********** qt - subtraction factors **********#
			MSlog = np.log(Q2 / facScale**2)
			mufsq = facScale**2

			#------------ squared amplitudes for qg-, gg-, and q aq-channels -------------#

			# Amplitude Squared q g -> h q, taken from ravindran,... Nucl.Phys.B 634 (2002) 247-290, eq. 2.13, 2.22
			amp2_qg_u = (coupl.alphas / np.pi)**3 * (np.pi**2 *( - ca * cf ) * ( Q2**2 + t**2 )) / (216. * v2 * u)
			
			amp2_qg_t = (coupl.alphas / np.pi)**3 * (np.pi**2 *( - ca * cf ) * ( Q2**2 + u**2 )) / (216. * v2 * t)

			# Amplitude Squared g g > h g, see Maltoni's document on Basics of QCD for the LHC
			num_M2_gg = coupl.alphas**3 * ( 2. * xt2new**2 * Q2**4  + (mH**2 - Q2)**4  - 4. * xt2new * Q2**2 *\
							(mH**2 - Q2)**2 + mH**8  + Q2**4)
				
			den_M2_gg = 24. * np.pi * v2 * xt2new * Q2**3
			
			amp2_gg = num_M2_gg / den_M2_gg

			# Amplitude Squared q aq -> h g, taken from Ravindran,... Nucl.Phys.B 634 (2002) 247-290,   eq. (2.13) - (2.19)
			amp2_qaq = (coupl.alphas / np.pi)**3 * (np.pi**2 * ca * cf * (t**2 + u**2)) / (81. * v2 * Q2)

			#------------- pdf combinations for Born cross sections -------------#

			born_pdf_qg_t = pdf_x1.glu * ( pdf_x2.up + pdf_x2.aup + pdf_x2.d + pdf_x2.ad \
							+ pdf_x2.c + pdf_x2.ac + pdf_x2.st \
							+ pdf_x2.ast + pdf_x2.b + pdf_x2.ab )

			born_pdf_qg_u = pdf_x2.glu * ( pdf_x1.up + pdf_x1.aup + pdf_x1.d + pdf_x1.ad \
							+ pdf_x1.c + pdf_x1.ac + pdf_x1.st \
							+ pdf_x1.ast + pdf_x1.b + pdf_x1.ab )

			born_pdf_gg = pdf_x1.glu * pdf_x2.glu

			
			born_pdf_qaq = pdf_x1.up * pdf_x2.aup + pdf_x2.up * pdf_x1.aup \
							+ pdf_x1.d * pdf_x2.ad + pdf_x2.d * pdf_x1.ad\
							+ pdf_x1.st * pdf_x2.ast + pdf_x2.st * pdf_x1.ast\
							+ pdf_x1.c * pdf_x2.ac + pdf_x2.c * pdf_x1.ac\
							+ pdf_x1.b * pdf_x2.ab + pdf_x2.b * pdf_x1.ab


			#---------- qaq - channel, Born  -----------#
			born_qaq =  GeV_pb * HypCubeFac * born_pdf_qaq * amp2_qaq * PS

			#---------- qg - channel, Born -----------#
			born_qg = GeV_pb * HypCubeFac * (amp2_qg_u * born_pdf_qg_u + amp2_qg_t * born_pdf_qg_t) * PS  

			#---------- gg - channel, Born -----------#
			born_gg =  GeV_pb * HypCubeFac * born_pdf_gg * amp2_gg * PS

			born = GeV_pb * HypCubeFac * PS  # without amplitude squared and pdfs

			HJet_LO = born_qaq + born_qg + born_gg

			hfunction_tot = 0.
			#******************************************************************************#
			#*************************** pdf combinations @ NLO ***************************#
			#******************************************************************************#

			quark_pdf_x1 = ( pdf_x1.up + pdf_x1.aup + pdf_x1.d + pdf_x1.ad \
					+ pdf_x1.c + pdf_x1.ac + pdf_x1.st \
					+ pdf_x1.ast + pdf_x1.b + pdf_x1.ab )
			
			
			quark_pdf_x2 = ( pdf_x2.up + pdf_x2.aup + pdf_x2.d + pdf_x2.ad \
					+ pdf_x2.c + pdf_x2.ac + pdf_x2.st \
					+ pdf_x2.ast + pdf_x2.b + pdf_x2.ab )

			quark_pdf_convo_x1 = ( pdf_x1_z1.up + pdf_x1_z1.aup + pdf_x1_z1.d + pdf_x1_z1.ad \
					+ pdf_x1_z1.c + pdf_x1_z1.ac + pdf_x1_z1.st \
					+ pdf_x1_z1.ast + pdf_x1_z1.b + pdf_x1_z1.ab )

			quark_pdf_convo_x2 = ( pdf_x2_z2.up + pdf_x2_z2.aup + pdf_x2_z2.d + pdf_x2_z2.ad \
					+ pdf_x2_z2.c + pdf_x2_z2.ac + pdf_x2_z2.st \
					+ pdf_x2_z2.ast + pdf_x2_z2.b + pdf_x2_z2.ab )


			

			pdf_aq_x2_q_x1 = pdf_x1.up * pdf_x2.aup + pdf_x1.d * pdf_x2.ad \
									+ pdf_x1.st * pdf_x2.ast + pdf_x1.c * pdf_x2.ac \
									+ pdf_x1.b * pdf_x2.ab

			pdf_aq_x1_q_x2 = pdf_x2.up * pdf_x1.aup + pdf_x2.d * pdf_x1.ad  \
											+ pdf_x2.st * pdf_x1.ast + pdf_x2.c * pdf_x1.ac \
											+ pdf_x2.b * pdf_x1.ab
			

			pdf_aq_convo_x2 = pdf_x1.up * pdf_x2_z2.aup + pdf_x1.d * pdf_x2_z2.ad \
									+ pdf_x1.st * pdf_x2_z2.ast + pdf_x1.c * pdf_x2_z2.ac \
									+ pdf_x1.b * pdf_x2_z2.ab

			pdf_aq_convo_x1 = pdf_x2.up * pdf_x1_z1.aup + pdf_x2.d * pdf_x1_z1.ad \
									+ pdf_x2.st * pdf_x1_z1.ast + pdf_x2.c * pdf_x1_z1.ac \
									+ pdf_x2.b * pdf_x1_z1.ab

			pdf_q_convo_x1 =  pdf_x1_z1.up * pdf_x2.aup + pdf_x1_z1.d * pdf_x2.ad \
								+ pdf_x1_z1.st * pdf_x2.ast + pdf_x1_z1.c * pdf_x2.ac \
								+ pdf_x1_z1.b * pdf_x2.ab

			pdf_q_convo_x2 = pdf_x2_z2.up * pdf_x1.aup + pdf_x2_z2.d * pdf_x1.ad \
									+ pdf_x2_z2.st * pdf_x1.ast + pdf_x2_z2.c * pdf_x1.ac\
									+ pdf_x2_z2.b * pdf_x1.ab


			q_pdf_x1 = ( pdf_x1.up + pdf_x1.d + pdf_x1.c + pdf_x1.st + pdf_x1.b )

			q_pdf_x2 = ( pdf_x2.up + pdf_x2.d + pdf_x2.c + pdf_x2.st + pdf_x2.b )

			aq_pdf_x1 = (pdf_x1.aup + pdf_x1.ad + pdf_x1.ac + pdf_x1.ast + pdf_x1.ab)

			aq_pdf_x2 = (pdf_x2.aup + pdf_x2.ad + pdf_x2.ac + pdf_x2.ast + pdf_x2.ab)

			q_convo_pdf_x1 = ( pdf_x1_z1.up + pdf_x1_z1.d + pdf_x1_z1.c \
									+ pdf_x1_z1.st + pdf_x1_z1.b )

			q_convo_pdf_x2 = ( pdf_x2_z2.up + pdf_x2_z2.d + pdf_x2_z2.c \
									+ pdf_x2_z2.st + pdf_x2_z2.b )

			aq_convo_pdf_x1 = (pdf_x1_z1.aup + pdf_x1_z1.ad + pdf_x1_z1.ac \
										+ pdf_x1_z1.ast + pdf_x1_z1.ab)

			aq_convo_pdf_x2 = (pdf_x2_z2.aup + pdf_x2_z2.ad + pdf_x2_z2.ac \
										+ pdf_x2_z2.ast + pdf_x2_z2.ab)

			if qg_channel:
				#****************************************************************************************#
				#********************************** Begin qg - channel **********************************#
				#****************************************************************************************#

				#------------------------------ Hard Factor H1_qg, qg- channel ------------------------------#
				
				I_hard1_qg_channel = born * (amp2_qg_u * coupl.alphas / np.pi * hard_factor_qg(R_jet,renScale,Q2,t,u) * born_pdf_qg_u \
								+ amp2_qg_t * coupl.alphas / np.pi * hard_factor_qg(R_jet,renScale,Q2,u,t)* born_pdf_qg_t) 
				
				#------------------------------ MS bar, Pgg splitting, Born: q g > h q ------------------------------#

				#  as / pi expansion convention for Pgg    
				Pgg_conv_0_x2 = ca * pdf_x2.glu * np.log(1. - x2) 
				
				Pgg_conv_x2_1 =  (-np.log(x2) * z2new) * (ca * (pdf_x2_z2.glu / z2new - pdf_x2.glu) / (1. - z2new) \
								+ ca * pdf_x2_z2.glu / z2new * ((1. - 2. * z2new) / z2new + z2new * (1. - z2new))) \
								- bg1 / 2. * pdf_x2.glu

				Pgg_conv_0_x1 = ca * pdf_x1.glu * np.log(1. - x1) 
				
				Pgg_conv_x1_1 = (-np.log(x1) * z1new) * (ca * (pdf_x1_z1.glu / z1new - pdf_x1.glu) / (1. - z1new) \
								+ ca * pdf_x1_z1.glu / z1new * ((1. - 2. * z1new) / z1new + z1new * (1. - z1new))) \
								- bg1 / 2. * pdf_x1.glu

				I_msbar_gg_qg_channel =  coupl.alphas / np.pi * MSlog * born * \
					(amp2_qg_u * quark_pdf_x1 * ( Pgg_conv_0_x2 +  Pgg_conv_x2_1 ) + amp2_qg_t * quark_pdf_x2 * ( Pgg_conv_0_x1 + Pgg_conv_x1_1 ) )  
				
				#------------------------------ MS bar, Pqq splitting, Born: q g > h q ------------------------------#
				
				# Convoluted Piecem note: as / 2pi expansion convention used here for the Pqq spl. function

				Pqq_convo_x1_1 = (-np.log(x1) * z1new) * cf * ( (1. + z1new**2 ) / z1new * quark_pdf_convo_x1  - 2. * quark_pdf_x1 ) / (1. - z1new) \
								+ 2. * cf * quark_pdf_x1 * np.log(1. - x1) + 1.5 * cf * quark_pdf_x1

				Pqq_convo_x2_1 = (-np.log(x2) * z2new) * cf * ( (1. + z2new**2 ) / z2new * quark_pdf_convo_x2  - 2. * quark_pdf_x2 ) / (1. - z2new) \
								+ 2. * cf * quark_pdf_x2 * np.log(1. - x2) + 1.5 * cf * quark_pdf_x2
				


				I_msbar_qq_qg_channel = coupl.alphas / (2. * np.pi) * MSlog * born * ( amp2_qg_u * pdf_x2.glu *  Pqq_convo_x1_1 \
						+ amp2_qg_t * pdf_x1.glu *  Pqq_convo_x2_1 ) 


				#------------------------------ MS bar, Pgq splitting, Born: g g > h g ------------------------------#

				# convolution with Pgq splitting function (q -> g), note: as / pi splitting function convention

				Pgq_convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * 0.5 * cf * (1. + (1. - z1new)**2) / z1new * quark_pdf_convo_x1

				Pgq_convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * 0.5 *cf * (1. + (1. - z2new)**2) / z2new * quark_pdf_convo_x2

				I_msbar_gq_qg_channel = coupl.alphas / (np.pi) * MSlog * born * amp2_gg \
									* ( pdf_x1.glu * Pgq_convo_x2 + pdf_x2.glu * Pgq_convo_x1 ) 

				#------------------------------ MS bar, Pqg splitting, Born: q aq > h g ------------------------------#
				
				# convolution of gluon pdf with Pqg (g -> q) splitting function, note as / pi expansion convention

				Pqg_convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * 0.5 * tr * (z2new**2 + (1. - z2new)**2) * pdf_x2_z2.glu

				Pqg_convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * 0.5 * tr * (z1new**2 + (1. - z1new)**2) * pdf_x1_z1.glu
				

				I_msbar_qg_qg_channel = coupl.alphas / (np.pi) * MSlog * born * amp2_qaq \
											* ( quark_pdf_x1 * Pqg_convo_x2 + quark_pdf_x2 * Pqg_convo_x1 ) 


				#------------------------------ beam function, Pqq splitting, Born: q g > h q ------------------------------#

				# Cqq beam functions (as/pi expansion convention, see e.g. phd th. L.B.)

				Cqq_z1 = 0.5 * cf * (1. - z1new)
				Cqq_z2 = 0.5 * cf * (1. - z2new)

				# convolution beam function, quark pdfs

				Cqq_convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * Cqq_z1 * quark_pdf_convo_x1
				Cqq_convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * Cqq_z2 * quark_pdf_convo_x2

				I_qq_beam_qg_channel = coupl.alphas / np.pi * born * (amp2_qg_u * pdf_x2.glu * Cqq_convo_x1 \
																							+ amp2_qg_t * pdf_x1.glu * Cqq_convo_x2)  

				#------------------------------ beam function, Pgq splitting, Born: g g > h g ------------------------------#
			
				# convolution with Cgq beam functions (as / pi expansion convention)

				Cgq_z1 = 0.5 * cf * z1new
				Cgq_z2 = 0.5 * cf * z2new

				Cgq_convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * Cgq_z1 * quark_pdf_convo_x1

				Cgq_convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * Cgq_z2 * quark_pdf_convo_x2

				I_gq_beam_qg_channel = coupl.alphas / (np.pi) * born * amp2_gg  \
									* ( pdf_x1.glu * Cgq_convo_x2 + pdf_x2.glu * Cgq_convo_x1 )


				#------------------------------ beam function, Pqg splitting, Born: q aq > h g ------------------------------#
				# convolution of gluon pdf with Cqg beam function (as / pi expansion convention)

				Cqg_z1 = 0.5 * z1new * (1. - z1new)
				Cqg_z2 = 0.5 * z2new * (1. - z2new)


				Cqg_convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * Cqg_z2 * pdf_x2_z2.glu

				Cqg_convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * Cqg_z1 * pdf_x1_z1.glu
				
				I_qg_beam_qg_channel = coupl.alphas / (np.pi) * born * amp2_qaq \
								* ( quark_pdf_x1 * Cqg_convo_x2 + quark_pdf_x2 * Cqg_convo_x1 )

				I_qg_channel = I_hard1_qg_channel + I_msbar_gg_qg_channel + I_msbar_qq_qg_channel + I_msbar_gq_qg_channel + I_msbar_qg_qg_channel\
									+ I_qq_beam_qg_channel + I_gq_beam_qg_channel + I_qg_beam_qg_channel
				

				hfunction_tot += born_qg + I_qg_channel
				#**************************************************************************************#
				#********************************** End qg - channel **********************************#
				#**************************************************************************************#


			if qq_channel:
				#*****************************************************************************************#
				#********************************** Begin qq' - channel **********************************#
				#*****************************************************************************************#

				#------------------------------ Hard Factor H1_qq, qq- channel ------------------------------#

				I_hard1_qq_channel = coupl.alphas / np.pi * born * born_pdf_qaq * amp2_qaq  \
										* hard_factor_qq(R_jet,renScale,Q2,t,u)

				#------------------------------ MS bar, Pqq splitting, Born: q aq > g h ------------------------------#

				# Pqq splitting kernel in the as / pi convention

				Pqq_z1 = 0.5 * cf * (1. + z1new**2) / (1. - z1new)
				Pqq_z2 = 0.5 * cf * (1. + z2new**2) / (1. - z2new)

				# quark splitting
				qq_convo_x1 = (-np.log(x1) * z1new) * Pqq_z1 * ( pdf_q_convo_x1 / z1new  - pdf_aq_x2_q_x1) \
									+ 0.5 * cf * pdf_aq_x2_q_x1 * np.log(1. - x1)

				qq_convo_x2 = (-np.log(x2) * z2new) * Pqq_z2 * ( pdf_q_convo_x2 / z2new  - pdf_aq_x1_q_x2) \
									+ 0.5 * cf * pdf_aq_x1_q_x2 * np.log(1. - x2)

				I_split_quark = coupl.alphas / np.pi * MSlog * born * amp2_qaq * ( qq_convo_x1 + qq_convo_x2) 

				# anti-quark splitting

				aqaq_convo_x1 = (-np.log(x1) * z1new) * Pqq_z1 * ( pdf_aq_convo_x1 / z1new  - pdf_aq_x1_q_x2) \
									+ 0.5 * cf * pdf_aq_x1_q_x2 * np.log(1. - x1)

				aqaq_convo_x2 = (-np.log(x2) * z2new) * Pqq_z2 * ( pdf_aq_convo_x2 / z2new  - pdf_aq_x2_q_x1) \
									+ 0.5 * cf * pdf_aq_x2_q_x1 * np.log(1. - x2)


				I_split_aquark = coupl.alphas / np.pi * MSlog * born * amp2_qaq * ( aqaq_convo_x1 + aqaq_convo_x2 )
				
				I_msbar_qq_qq_channel = I_split_quark + I_split_aquark

				#------------------------------ MS bar, Pgq splitting, Born: q g > q h ------------------------------#
				
				# Pgq splitting kernel, as / pi convention

				Pgq_z1 = 0.5 * cf * (1. + (1. - z1new)**2) / z1new
				Pgq_z2 = 0.5 * cf * (1. + (1. - z2new)**2) / z2new

				# quark - a-quark, quark splitting

				q_split_x1 = (-np.log(x1) * z1new) * 1. / z1new * Pgq_z1 * q_convo_pdf_x1 # u-channel

				q_split_x2 = (-np.log(x2) * z2new) * 1. / z2new * Pgq_z2 * q_convo_pdf_x2 # t-channel

				I_aqq_q_split = coupl.alphas / np.pi * born * ( aq_pdf_x2 * q_split_x1 * amp2_qg_u \
																					+ aq_pdf_x1 * q_split_x2 * amp2_qg_t) * MSlog

				# quark - a-quark, a-quark splitting

				aq_split_x1 = (-np.log(x1) * z1new) * 1. / z1new * Pgq_z1 * aq_convo_pdf_x1 # t-channel

				aq_split_x2 = (-np.log(x2) * z2new) * 1. / z2new * Pgq_z2 * aq_convo_pdf_x2 # u-channel

				I_aqq_aq_split = coupl.alphas / np.pi * born * ( q_pdf_x1 * aq_split_x2 * amp2_qg_u \
																					+ q_pdf_x2 * aq_split_x1 * amp2_qg_t) * MSlog


				# quark - quark, quark splitting

				I_qq_q_split = coupl.alphas / np.pi * born * ( q_pdf_x1  * q_split_x2 * amp2_qg_u \
																					+ q_pdf_x2 * q_split_x1 * amp2_qg_t ) * MSlog

				# a-quark - a-quark, a-quark splitting


				I_aqaq_aq_split = coupl.alphas / np.pi * born * ( aq_pdf_x1  * aq_split_x2 * amp2_qg_u \
																					+ aq_pdf_x2 * aq_split_x1 * amp2_qg_t ) * MSlog


				I_msbar_gq_qq_channel = I_aqq_q_split + I_aqq_aq_split + I_qq_q_split + I_aqaq_aq_split

				#------------------------------ beam function, Pqq splitting, Born: q aq > h g ------------------------------#
			
				# convoluted piece
				# Cqq beam functions in the as / pi convention
				Cqq_z1 = 0.5 * cf * (1. - z1new)
				Cqq_z2 = 0.5 * cf * (1. - z2new)

				# quark splitting
				q_convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * Cqq_z1 * pdf_q_convo_x1
				q_convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * Cqq_z2 * pdf_q_convo_x2

				I_split_quark = coupl.alphas / np.pi * born * amp2_qaq * (q_convo_x1 + q_convo_x2)

				# anti-quark splitting
				aq_convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * Cqq_z1 * pdf_aq_convo_x1
				aq_convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * Cqq_z2 * pdf_aq_convo_x2

				I_split_aquark = coupl.alphas / np.pi * born * amp2_qaq * (aq_convo_x1 + aq_convo_x2)

				
				I_qq_beam_qq_channel = I_split_quark + I_split_aquark


				#------------------------------ beam function, Pgq splitting, Born: q g > h q ------------------------------#
				
				# Cgq beam functions , as / pi convention

				Cgq_z1 = 0.5 * cf * z1new
				Cgq_z2 = 0.5 * cf * z2new

				# quark - a-quark, quark splitting

				q_split_x1 = (-np.log(x1) * z1new) * 1. / z1new * Cgq_z1 * q_convo_pdf_x1 # u-channel

				q_split_x2 = (-np.log(x2) * z2new) * 1. / z2new * Cgq_z2 * q_convo_pdf_x2 # t-channel

				I_aqq_q_split = coupl.alphas / np.pi * born * ( aq_pdf_x2 * q_split_x1 * amp2_qg_u \
																					+ aq_pdf_x1 * q_split_x2 * amp2_qg_t)

				# quark - a-quark, a-quark splitting

				aq_split_x1 = (-np.log(x1) * z1new) * 1. / z1new * Cgq_z1 * aq_convo_pdf_x1 # t-channel

				aq_split_x2 = (-np.log(x2) * z2new) * 1. / z2new * Cgq_z2 * aq_convo_pdf_x2 # u-channel

				I_aqq_aq_split = coupl.alphas / np.pi * born * ( q_pdf_x1 * aq_split_x2 * amp2_qg_u \
																					+ q_pdf_x2 * aq_split_x1 * amp2_qg_t)


				# quark - quark, quark splitting

				I_qq_q_split = coupl.alphas / np.pi * born * ( q_pdf_x1  * q_split_x2 * amp2_qg_u \
																					+ q_pdf_x2 * q_split_x1 * amp2_qg_t ) 

				# a-quark - a-quark, a-quark splitting


				I_aqaq_aq_split = coupl.alphas / np.pi * born * ( aq_pdf_x1  * aq_split_x2 * amp2_qg_u \
																					+ aq_pdf_x2 * aq_split_x1 * amp2_qg_t ) 



				I_gq_beam_qq_channel = I_aqq_q_split + I_aqq_aq_split + I_qq_q_split + I_aqaq_aq_split


				I_qq_channel = I_hard1_qq_channel + I_msbar_qq_qq_channel + I_msbar_gq_qq_channel + I_qq_beam_qq_channel + I_gq_beam_qq_channel


				hfunction_tot += born_qaq + I_qq_channel
				#***************************************************************************************#
				#********************************** End qq' - channel **********************************#
				#***************************************************************************************#

			if gg_channel:
				#****************************************************************************************#
				#********************************** begin gg - channel **********************************#
				#****************************************************************************************#

				#------------------------------ Hard Factor H1_gg, gg- channel ------------------------------#

				I_hard1_gg_channel  = GeV_pb * born_pdf_gg * PS * amp2_gg * HypCubeFac * coupl.alphas / np.pi * hard_factor_gg(R_jet,renScale,Q2,t,u)

				#------------------------------ MS bar, Pgg splitting, Born: g g > g h ------------------------------#

				conv1_HypCubeFac = (-np.log(x1) * z1new)
				conv2_HypCubeFac = (-np.log(x2) * z2new)
				
				# Convolutions

				# z1 convolution

				conv_x1_1  = (ca * (pdf_x1_z1.glu / z1new - pdf_x1.glu) / (1. - z1new) \
								+ ca * pdf_x1_z1.glu / z1new * ( (1. - 2. * z1new) / z1new + z1new * (1. - z1new)))

				conv_0_x1 = ca * pdf_x1.glu * np.log(1. - x1)
				
				# z2 convolution

				conv_0_x2 = ca * pdf_x2.glu * np.log(1. - x2)

				conv_x2_1 = (ca * (pdf_x2_z2.glu / z2new - pdf_x2.glu) / (1. - z2new) \
								+ ca * pdf_x2_z2.glu / z2new * ((1. - 2. * z2new) / z2new + z2new * (1. - z2new)))
				
				
				I_msbar_gg_gg_channel = coupl.alphas / (np.pi) * born * amp2_gg * ( pdf_x2.glu * ( conv_0_x1 \
						+ conv_x1_1 * conv1_HypCubeFac  - 0.5 * bg1 * pdf_x1.glu) + pdf_x1.glu * ( conv_0_x2 \
						+ conv_x2_1 * conv2_HypCubeFac  - 0.5 * bg1 * pdf_x2.glu)) * MSlog


				#------------------------------ MS bar + beam function, Pgq splitting, Born: q g > q h ------------------------------#
				
				HypCubeFac_conv1 = (-np.log(x1) * z1new)
				HypCubeFac_conv2 = (-np.log(x2) * z2new)
				
				
				# ms bar Convolutions

				splitting_kernel_qg_1 = 0.5 * tr * (z1new**2 + (1 - z1new)**2)

				splitting_kernel_qg_2 = 0.5 * tr * (z2new**2 + (1 - z2new)**2)

				conv_x1_1 = 1. / z1new * splitting_kernel_qg_1 * pdf_x1_z1.glu

				conv_x2_1 = 1. / z2new * splitting_kernel_qg_2 * pdf_x2_z2.glu

				
				Ims =   coupl.alphas / (np.pi) * ( born ) * ( amp2_qg_u * pdf_x2.glu * conv_x1_1 * HypCubeFac_conv1 \
					+ amp2_qg_t * pdf_x1.glu * conv_x2_1 * HypCubeFac_conv2 ) * MSlog * 2. * nf


				# beam function convolutions

				beam_function_qg_1 = 0.5 * tr * (2. * z1new * (1 - z1new) )
				beam_function_qg_2 = 0.5 * tr * (2. * z2new * (1 - z2new) )

				conv_x1_1_beam = 1. / z1new * beam_function_qg_1 * pdf_x1_z1.glu

				conv_x2_1_beam = 1. / z2new * beam_function_qg_2 * pdf_x2_z2.glu

				Ibeam = coupl.alphas / (np.pi) * ( born ) * (amp2_qg_u * pdf_x2.glu * conv_x1_1_beam * HypCubeFac_conv1 \
					+ amp2_qg_t * pdf_x1.glu * conv_x2_1_beam * HypCubeFac_conv2) * 2. * nf

				
				I_msbar_plus_beam_gq_gg_channel = Ims + Ibeam

				I_gg_channel = I_hard1_gg_channel + I_msbar_gg_gg_channel + I_msbar_plus_beam_gq_gg_channel


				hfunction_tot += born_gg + I_gg_channel
				#**************************************************************************************#
				#********************************** end gg - channel **********************************#
				#**************************************************************************************#

			# HJet_NLO_Hunction = HJet_LO + I_qg_channel + I_qq_channel + I_gg_channel

			if writehisto:
				if iter[0] > 2 :
						iterations.append(iter[0])
						wgt_vegas.append(weight[0])
						integrand_wgt.append(hfunction_tot)


						#--- 2 -> 2 Kin., p(P1) p(P2) -> j(p3) + H(p4)

						E3 = np.sqrt(Q2) / 2. * (1. - mH**2 / Q2)
						E4 = np.sqrt(Q2) / 2. * (1. + mH**2 / Q2)

						#--- momenta in partonic frame ---#

						p1 = np.sqrt(Q2) / 2. * np.array([1,0,0,1])
						p2 = np.sqrt(Q2) / 2. * np.array([1,0,0,-1])
						b = 1. 
						if np.random.rand() < 0.5:
							b = -1.

						p3 = E3 * np.array([1,  b * np.sqrt(- 2. * t / (np.sqrt(Q2) * E3) * (1. + 0.5 * t / (np.sqrt(Q2) * E3))) * np.cos(phinew), \
								b * np.sqrt(- 2. * t / (np.sqrt(Q2) * E3) * (1. + 0.5 * t / (np.sqrt(Q2) * E3))) * np.sin(phinew) , \
								1. + t / (np.sqrt(Q2) * E3)])

						p4 = p1 + p2 - p3

						#--- momenta in hadronic frame ---#
						refvec = np.sqrt(S) / 2. * np.array([x1 + x2,0,0,x2 - x1])

						p1had = longi_boost(p1,refvec)
						p2had = longi_boost(p2,refvec)
						p3had = longi_boost(p3,refvec)
						p4had = p1had + p2had - p3had

						#-- Observables --#

						yH  = 0.5 * np.log((p4had[0] + p4had[3]) / (p4had[0] - p4had[3]))
						yJet = 0.5 * np.log((p3had[0] + p3had[3]) / (p3had[0] - p3had[3]))

						ptH = np.sqrt(p4had[1]**2 + p4had[2]**2)
						ptJet = np.sqrt(p3had[1]**2 + p3had[2]**2)
						qt = np.sqrt((p4had[1]+p3had[1])**2 + (p4had[2]+p3had[2])**2)

						mH_jet = np.sqrt((p4had[0] + p3had[0])**2 - (p4had[1] + p3had[1])**2 - (p4had[2] + p3had[2])**2 - (p4had[3] + p3had[3])**2)
						#--- save observables values for distributions ---#

						rap_higgs.append(yH)

						pt_Higgs.append(ptH)

						pt_jet.append(ptJet)

						m_higgs_jet.append(mH_jet)

						pt_higgs_jet.append(qt)


			ff[0] = hfunction_tot

			return 0



		NDIM = 6  
		EPSREL = ep_rel
		NINCREASE = N_increase
		MAXEVAL = max_eval
		
		
		
		def print_header(name):
			print("")
			print('#****************************** %s ***************************** #' % name)
		def print_results(name, results):
			keys = ['nregions', 'neval', 'fail']
			text = ["%s %d" % (k, results[k]) for k in keys if k in results]
			print("%s RESULt:\t" % name.upper() + "\t".join(text))
			for comp in results['results']:
				print("%s RESULT:\t" % name.upper() + \
					"%(integral).8f +- %(error).8f\tp = %(prob).3f\n" % comp)

		from os import environ as env
		verbose = 2
		if 'CUBAVERBOSE' in env:
			verbose = int(env['CUBAVERBOSE'])

		print_header('Vegas')
		print_results('Vegas', pycuba.Vegas(Integrand, NDIM,epsrel=EPSREL,maxeval=MAXEVAL,\
							nincrease=NINCREASE, verbose=2))

		


	if writehisto:
		wgts = np.array(wgt_vegas) * np.array(integrand_wgt) / (iterations[-1] - 2)

		rapH = myHisto("Higgs Rapidity")
		rapH.fill(rap_higgs,-5.,5.,0.4,wgts / 0.4)
		np.savez("raphiggs_hf",hist = rapH.histo ,binedges = rapH.binedges ,hist_err = rapH.error)

		pth = myHisto("Higgs pt")
		pth.fill(pt_Higgs,0.,1200.,15,wgts/15.)
		np.savez("pthiggs_hf",hist = pth.histo ,binedges = pth.binedges ,hist_err = pth.error)

		ptj = myHisto('jet pt')
		ptj.fill(pt_jet,0.,1200.,15.,wgts/15.)
		np.savez("ptjet_hf",hist = ptj.histo ,binedges = ptj.binedges ,hist_err = ptj.error)


		mHj = myHisto('Higgs jet invariant mass')
		mHj.fill(m_higgs_jet,0.,1200.,15.,wgts/15.)
		np.savez("mass_higgs_jet_hf",hist = mHj.histo ,binedges = mHj.binedges ,hist_err = mHj.error)

		qt_hj = myHisto('qt imbalance')
		qt_hj.fill(pt_higgs_jet,0.,1200.,15.,wgts/15.)
		np.savez("qt_imbalance_hf",hist = qt_hj.histo ,binedges = qt_hj.binedges ,hist_err = qt_hj.error)

		



