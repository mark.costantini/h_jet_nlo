#!/usr/bin/env python
try:
	import pycuba
	lesgo = True
except:
	print("Error: pycuba cannot be imported correctly.")
	print("The instructions to install PyCuba can be found on the following link")
	print("https://johannesbuchner.github.io/PyMultiNest/install.html")
	print("For more informations you can consult the documentation in hj_nlo/doc")
	lesgo = False


if lesgo:
	if __name__ == '__main__':

		import argparse
		import numpy as np
		import sys
		sys.path.insert(0,'../lib/')
		from constants import mH, v2, GeV_pb, ca, cf, tr, ag1, bg1, nf
		from col_alg_qghq import T1_sq_qg, T2_sq_qg, T13_qg, T23_qg
		from col_alg_qqhg import T1_sq_qq, T2_sq_qq, T13_qq, T23_qq
		from col_alg_gghg import T2_gg, T13_gg, T23_gg
		from boost_function import longi_boost
		from pdf import myPdf
		from myHisto import myHisto




		#----------- parser -----------#
		parser = argparse.ArgumentParser(description = 'set the value of the parameters' )

		parser.add_argument('--no-gg',dest='gg_ch',action='store_false')
		parser.add_argument('--no-qg',dest='qg_ch',action='store_false')
		parser.add_argument('--no-qq',dest='qq_ch',action='store_false')

		parser.add_argument('--pdf','-p',dest = 'pdf_set',nargs='?',default = 'NNPDF31_nlo_as_0118',type = str, help = "default: 'NNPDF31_nlo_as_0118' ")
		parser.add_argument('--energy','-e',dest = 'sqrtS_hadronic', nargs = '?',default = 13000. ,type = float, help = "default: 13000.")
		parser.add_argument('--pt_min_jet','-pt',dest = 'pt_min_jet', nargs = '?', default = 30., type = float, help = "default: 30.")
		parser.add_argument('--jet_radius','-rad',dest = 'jet_radius', nargs = '?', default = 0.1, type = float, help = "default: 0.1")
		parser.add_argument('--fac_scale','-muf',dest = 'fac_scale', nargs = '?', default = mH, type = float, help = "default: 125.1")
		parser.add_argument('--ren_scale','-mur',dest = 'ren_scale', nargs = '?', default = mH, type = float, help = "default: 125.1")
		parser.add_argument('--rcut','-cut',dest = 'cut', nargs = '?', default = 0.01, type = float, help = "rcut = qT/Q, default: 0.01")
		parser.add_argument('--leadingR','-lR',dest = 'full_R',action = 'store_false')
		parser.add_argument('--mm_and_soft','-mm_s',dest = 'mismatch_and_soft',nargs = '?',default = 0, type = int, help = "positional argument: 0 (default)-> Rjet=0.1, 1->Rjet=0.3,2->Rjet=0.8" )
		parser.add_argument('--ep_rel',dest='ep_rel',nargs='?',default=1e-3,type = float,help= "relative precision of MC integration, default: 1e-3")
		parser.add_argument('--N_increase','-N_inc',dest='N_increase',nargs='?',default=1000,type = int,help= "increase number of points per iterations, type = int, default: 1000")
		parser.add_argument('--max_eval','-max',dest='max_eval',nargs='?',default=10000,type = int,help= "max number of MC evaluations, type =int, default: 10000")

		parser.add_argument('--write_histo',dest='write_hist',action='store_true')
		parser.add_argument("--name_npz",'-npz',dest='name_hist_file',nargs = '?',default = "histo_hjet_ct",type=str,help = "default name of npz file is histo_hjet_ct")


		args = parser.parse_args()




		#--- pdf set ---#
		pdfSet = args.pdf_set

		#--- kinematics ---#
		S = (args.sqrtS_hadronic)**2
		qt_min_jet = args.pt_min_jet
		R_jet = args.jet_radius
		facScale = args.fac_scale
		renScale = args.ren_scale
		tau0 = 2. * np.sqrt(( mH**2 * qt_min_jet**2 + qt_min_jet**4)/S**2) + ( mH**2 + 2. * qt_min_jet**2 )/S # Minimal Value of tau
		cut = args.cut

		#--- soft and mismatch intergals for R = 0.1,0.3,0.8 ---#
		full_R = args.full_R
		if full_R:
				Radiusjet, mismatch_integral, soft_integral = np.loadtxt("../lib/mismatch_soft.txt",unpack = True)
		else:
				Radiusjet, mismatch_integral, soft_integral = np.loadtxt("../lib/mismatch_soft_leading.txt",unpack = True) 
		position_mm_and_soft = args.mismatch_and_soft

		#--------- channels ---------#
		gg_channel = args.gg_ch
		qg_channel = args.qg_ch
		qq_channel = args.qq_ch

		#--------- MC integration ---------#
		ep_rel = args.ep_rel
		N_increase = args.N_increase
		max_eval = args.max_eval

		#------ write output ------#
		writehisto = args.write_hist

		#------ output file for histo -------#
		name_npz = args.name_hist_file


		pdf_x1 = myPdf(pdfSet)
		pdf_x2 = myPdf(pdfSet)
		coupl = myPdf(pdfSet)
		pdf_x1_z1 = myPdf(pdfSet)
		pdf_x2_z2 = myPdf(pdfSet)

		print("")
		print("")
		print("#********************************************************************#")
		print("#                                  _ _        _ _                    #")
		print("#                     |  |   |    |   | |    |   |                   #")
		print("#                     |--|   | -- |   | |    |   |                   #")
		print("#                     |  | __|    |   | |_ _ |_ _|                   #")
		print("#                                                                    #")
		print("#********************************************************************#")
		print("")
		print("")
		print("#********************************************************************#")
		print("#                                                                    #")
		print("#                p p -> H + Jet @ NLO, CounterTerm                   #")
		print("#                         qT  Subtraction                            #")
		print("#																	#")
		print("#        all channels, i.e. qq', qg and gg are included              #")
		print("#               in the Integrand function                            #")
		print("#                                                                    #")
		print("#********************************************************************#")
		print("")
		print("#----------------------------- pdf set ------------------------------#")
		print("")
		print("%s"%(pdfSet))
		print("")
		print("#---------------------- kinematical variables -----------------------#")
		print("")
		print("Hadronic com energy sqrt(S) = %s GeV"%(np.sqrt(S))) 
		print("ptjet_min = %s GeV"%(qt_min_jet))
		print("full R dependece = %s"%(full_R))
		print("jetRadius = %s"%(R_jet))
		print("renScale = %s GeV, facScale = %s GeV"%(renScale,facScale))
		print("rcut = %s"%(cut))
		print("")
		print("#------------------------ channels included -------------------------#")
		print("")
		print("gg channel: %s"%(gg_channel))
		print("qg channel: %s"%(qg_channel))
		print("qq channel: %s"%(qq_channel))
		print("")
		print("#--------------------------- write histo ----------------------------#")
		print("")
		print("%s"%(writehisto))
		print("")
		if writehisto:
			print("histo saved to file: %s.npz"%(name_npz))
			print("")
		print("#------------------------- MC parameters ---------------------------#")
		print("")
		print("relative precision: %s"%(ep_rel))
		print("N increase: %s"%(N_increase))
		print("Max evaluations %s"%(max_eval))
		print("")
		print("#*******************************************************************#")
		print("#*******************************************************************#")





		#--------- Initialize empty list for all quantities that could be plotted ---------#
		rap_higgs = []
		pt_Higgs = []
		pt_jet = []
		pt_higgs_jet = []
		m_higgs_jet = []

		# initialize empty list for weights given by Vegas integration
		wgt_vegas = []
		integrand_wgt = []
		iterations = []



		def Integrand(ndim, xx, ncomp, ff, userdata, nvec, core, weight, iter):

			tau,y,phi,xt2,z1,z2 = [xx[i] for i in range(ndim.contents.value)]

			#*******************************************************************************************#
			#*********************** sampling on Born and convolutions variables ***********************#
			#*******************************************************************************************#

			# Kinematical Variables
			# importance sampling on tau
			lntau = tau * np.log(tau0)
			taunew = np.exp(lntau)

			ynew = 0.5 * np.log(taunew) - np.log(taunew) * y
			phinew = 2. * np.pi * phi

			x1 = np.sqrt(taunew) * np.exp(ynew)
			x2 = np.sqrt(taunew) * np.exp(-ynew)

			# Importance sampling on z1 (z1 convolution integral)    
			lnz1 = z1 * np.log(x1)
			z1new = np.exp(lnz1)

			# Importance sampling on z2 (z2 convolution integral)
			lnz2 = z2 * np.log(x2)
			z2new = np.exp(lnz2)

			Q2 = taunew * S
			qt2max = (Q2 - mH**2)**2 / (4. * Q2)
			xt2max = qt2max / Q2
			xt2min = qt_min_jet**2 / Q2

			# importance sampling on xt2
			logxt2 = np.log(xt2min) + (np.log(xt2max) - np.log(xt2min)) * xt2
			xt2new = np.exp(logxt2)

			#********* evaluate all pdfs ***********#
			pdf_x1.eval_pdf(facScale**2,x1)
			pdf_x2.eval_pdf(facScale**2,x2)
			pdf_x1_z1.eval_pdf(facScale**2,x1 / z1new)
			pdf_x2_z2.eval_pdf(facScale**2,x2 / z2new)
			coupl.eval_coupl(renScale)

			#******************************************************************************************#
			#********************************** Born dependent stuff **********************************#
			#******************************************************************************************#

			#--- jacobian coming from the change of variable. (Cuba integration performed over Hypercube)
			HypCubeFac = (-np.log(tau0) * taunew) * (- np.log(taunew) ) * (2. * np.pi) * (np.log(xt2max) - np.log(xt2min)) * xt2new

			# Phase Space factor (qt spectrum), already includes the flux factor (p1 + p2 -> p3 + p4, p3^2 = 0, p4^2 = mH^2)
			PS = 1.  / (32. * np.pi**2 * (Q2)) * 1 / (np.sqrt(xt2max - xt2new))

			#--- from qt2 to t
			a = 1.

			if (np.random.rand() < 0.5):
				a = -1.

			t = - np.sqrt(Q2 * qt2max) * ( 1. - a * np.sqrt(1 - Q2 * xt2new / qt2max ) )

			u = - Q2 - t + mH**2

			#------------ squared Born amplitudes for qg-, gg-, and q aq-channels -------------#

			# Amplitude Squared q g -> h q, taken from ravindran, smith,Van Nerveen 2002 eq. 2.13, 2.22        
			amp2_qg_u = (coupl.alphas / np.pi)**3 * (np.pi**2 *( - ca * cf ) * ( Q2**2 + t**2 )) / (216. * v2 * u)

			amp2_qg_t = (coupl.alphas / np.pi)**3 * (np.pi**2 *( - ca * cf ) * ( Q2**2 + u**2 )) / (216. * v2 * t)

			# Amplitude Squared g g > h g, see Maltoni's document on Basics of QCD for the LHC
			num_M2_gg = coupl.alphas**3 * ( 2. * xt2new**2 * Q2**4  + (mH**2 - Q2)**4  - 4. * xt2new * Q2**2 *\
												(mH**2 - Q2)**2 + mH**8  + Q2**4)

			den_M2_gg = 24. * np.pi * v2 * xt2new * Q2**3

			amp2_gg = num_M2_gg / den_M2_gg

			# Amplitude Squared q aq -> h g, taken from Ravindran .. 2002  eq. (2.13) - (2.19)
			amp2_qaq = (coupl.alphas / np.pi)**3 * (np.pi**2 * ca * cf * (t**2 + u**2)) / (81. * v2 * Q2)

			#------------- pdf combinations for Born cross sections -------------#
			mufsq = facScale**2

			born_pdf_qg_t = pdf_x1.glu * ( pdf_x2.up + pdf_x2.aup + pdf_x2.d + pdf_x2.ad \
							+ pdf_x2.c + pdf_x2.ac + pdf_x2.st \
							+ pdf_x2.ast + pdf_x2.b + pdf_x2.ab )

			born_pdf_qg_u = pdf_x2.glu * ( pdf_x1.up + pdf_x1.aup + pdf_x1.d + pdf_x1.ad \
										+ pdf_x1.c + pdf_x1.ac + pdf_x1.st \
										+ pdf_x1.ast + pdf_x1.b + pdf_x1.ab )

			born_pdf_gg = pdf_x1.glu * pdf_x2.glu


			born_pdf_qaq = pdf_x1.up * pdf_x2.aup + pdf_x2.up * pdf_x1.aup \
										+ pdf_x1.d * pdf_x2.ad + pdf_x2.d * pdf_x1.ad\
										+ pdf_x1.st * pdf_x2.ast + pdf_x2.st * pdf_x1.ast\
										+ pdf_x1.c * pdf_x2.ac + pdf_x2.c * pdf_x1.ac\
										+ pdf_x1.b * pdf_x2.ab + pdf_x2.b * pdf_x1.ab



			#**********************************************************************#
			#*********************** qt-subtraction factors ***********************#
			#**********************************************************************#

			L1cut = np.log(1. / cut**2)
			L2cut = np.log(1. / cut**2)**2
			counterterm = 0.

			if qg_channel:
				#******************************************************************#
				#*********************** begin qg - channel ***********************#
				#******************************************************************#

				#------------------ Soft counterterm, qg - channel ------------------#
				# quark pdf s


				quark_pdf_x1 = ( pdf_x1.up + pdf_x1.aup + pdf_x1.d + pdf_x1.ad \
												+ pdf_x1.c + pdf_x1.ac + pdf_x1.st \
												+ pdf_x1.ast + pdf_x1.b + pdf_x1.ab )

				quark_pdf_x2 = ( pdf_x2.up + pdf_x2.aup + pdf_x2.d + pdf_x2.ad \
												+ pdf_x2.c + pdf_x2.ac + pdf_x2.st \
												+ pdf_x2.ast + pdf_x2.b + pdf_x2.ab )

				born_qg = GeV_pb * HypCubeFac * (amp2_qg_u * born_pdf_qg_u + amp2_qg_t * born_pdf_qg_t) * PS  
											
				# Born Decoupling
				# u-channel bdc
				Ibdc_u = - coupl.alphas / np.pi * ( L1cut / 2. ) * ( T1_sq_qg * np.log( t / u ) + T2_sq_qg * np.log( u / t ) )  # multiply by born in the end

				# t-channel bdc
				Ibdc_t = - Ibdc_u

				Ibdc = GeV_pb * HypCubeFac * (amp2_qg_u * Ibdc_u * born_pdf_qg_u + amp2_qg_t * Ibdc_t * born_pdf_qg_t) * PS  

				# Mismatch CounterTerm

				Imismatch = coupl.alphas / np.pi * ( - T1_sq_qg - T2_sq_qg ) * L1cut / 2. * mismatch_integral[position_mm_and_soft]

				# Decoupled Soft CounterTerm 

				prefactor = coupl.alphas / np.pi * ( - T13_qg - T23_qg) * L1cut / 2

				Idec_soft = prefactor * soft_integral[position_mm_and_soft]

				I_soft_qg = born_qg * ( Imismatch + Idec_soft ) + Ibdc 

				#------------------ initial state counterterm Pgq splitting, Born: g g > h g, qg - channel ------------------#

				born_gg =  GeV_pb * HypCubeFac * amp2_gg * PS


				# quark pdf's for convolutions


				pdf_convo_x1_1 = ( pdf_x1_z1.up + pdf_x1_z1.aup + pdf_x1_z1.d + pdf_x1_z1.ad \
											+ pdf_x1_z1.c + pdf_x1_z1.ac + pdf_x1_z1.st \
											+ pdf_x1_z1.ast + pdf_x1_z1.b + pdf_x1_z1.ab )
								
				pdf_convo_x2_1 = ( pdf_x2_z2.up + pdf_x2_z2.aup + pdf_x2_z2.d + pdf_x2_z2.ad \
												+ pdf_x2_z2.c + pdf_x2_z2.ac + pdf_x2_z2.st \
												+ pdf_x2_z2.ast + pdf_x2_z2.b + pdf_x2_z2.ab )
								

				# convolution with Pgq splitting function (q -> g), (as /  pi convention)

				convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * 0.5 * cf * (1. + (1. - z1new)**2) / z1new * pdf_convo_x1_1

				convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * 0.5 * cf * (1. + (1. - z2new)**2) / z2new * pdf_convo_x2_1

				L1cut = np.log(1. / cut**2)

				I_gq_splitting_qg_channel = coupl.alphas / ( np.pi) * born_gg * ( pdf_x1.glu * convo_x2 + pdf_x2.glu * convo_x1 ) * L1cut

				#------------------ initial state counterterm Pqg splitting, Born: q aq > h g, qg - channel ------------------#

				born_qaq =  GeV_pb * HypCubeFac * amp2_qaq * PS

				# convolution of gluon pdf with Pqg (g -> q) splitting function, (as / pi convention)

				convo_x2 = (-np.log(x2) * z2new) * 1. / z2new * 0.5 * tr * (z2new**2 + (1. - z2new)**2) * pdf_x2_z2.glu

				convo_x1 = (-np.log(x1) * z1new) * 1. / z1new * 0.5 * tr * (z1new**2 + (1. - z1new)**2) * pdf_x1_z1.glu

				I_qg_splitting_qg_channel = coupl.alphas / ( np.pi) * born_qaq * ( quark_pdf_x1 * convo_x2 + quark_pdf_x2 * convo_x1 ) * L1cut


				#------------------ initial state counterterm Pqq splitting, Born: q g > h q, qg - channel ------------------#

				born =   GeV_pb * HypCubeFac * PS # wo ampl squared


				# Non convoluted piece

				Iq1 = coupl.alphas / ( 2. * np.pi ) * born  * ( 0.5 * cf * L2cut - 1.5 * cf * L1cut) \
												* ( amp2_qg_t * pdf_x1.glu * quark_pdf_x2  + amp2_qg_u * pdf_x2.glu * quark_pdf_x1 )

				# Convoluted Piece

				qq_convo_x1_1 = (-np.log(x1) * z1new) * cf * ( (1. + z1new**2 ) / z1new * pdf_convo_x1_1  - 2. * quark_pdf_x1 ) / (1. - z1new) \
													+ 2. * cf * quark_pdf_x1 * np.log(1. - x1) + 1.5 * cf * quark_pdf_x1

				qq_convo_x2_1 = (-np.log(x2) * z2new) * cf * ( (1. + z2new**2 ) / z2new * pdf_convo_x2_1  - 2. * quark_pdf_x2 ) / (1. - z2new) \
											+ 2. * cf * quark_pdf_x2 * np.log(1. - x2) + 1.5 * cf * quark_pdf_x2



				Iq2 = coupl.alphas / (2. * np.pi) * born * ( amp2_qg_u * pdf_x2.glu *  qq_convo_x1_1 \
							+ amp2_qg_t * pdf_x1.glu *  qq_convo_x2_1 ) * L1cut



				I_qq_splitting_qg_channel = Iq1 + Iq2    

				#------------------ initial state counterterm Pgg splitting, Born: q g > h q, qg - channel ------------------#

				#  as / pi convention for Pgg 

				# Non convoluted piece

				Ig1 = coupl.alphas / np.pi * born * ( 1. / 4. * ag1 * L2cut + bg1 / 2. * L1cut) \
								* ( amp2_qg_u * quark_pdf_x1 * pdf_x2.glu + amp2_qg_t * quark_pdf_x2 * pdf_x1.glu )

				# Convoluted piece

				conv_0_x2 = ca * pdf_x2.glu * np.log(1. - x2) 

				conv_x2_1 =  (-np.log(x2) * z2new) * (ca * (pdf_x2_z2.glu / z2new - pdf_x2.glu) / (1. - z2new) \
											+ ca * pdf_x2_z2.glu / z2new * ((1. - 2. * z2new) / z2new + z2new * (1. - z2new))) \
											- bg1 / 2. * pdf_x2.glu

				conv_0_x1 = ca * pdf_x1.glu * np.log(1. - x1)

				conv_x1_1 = (-np.log(x1) * z1new) * (ca * (pdf_x1_z1.glu / z1new - pdf_x1.glu) / (1. - z1new) \
											+ ca * pdf_x1_z1.glu / z1new * ((1. - 2. * z1new) / z1new + z1new * (1. - z1new))) \
											- bg1 / 2. * pdf_x1.glu


				Ig2 =  coupl.alphas / np.pi * born * \
									(amp2_qg_u * quark_pdf_x1 * ( conv_0_x2 +  conv_x2_1 ) + amp2_qg_t * quark_pdf_x2 * ( conv_0_x1 + conv_x1_1 ) ) * L1cut 

				I_gg_splitting_qg_channel = Ig1 + Ig2



				Iqg_channel = I_soft_qg + I_gq_splitting_qg_channel + I_qg_splitting_qg_channel + I_qq_splitting_qg_channel + I_gg_splitting_qg_channel


				counterterm += Iqg_channel
				#****************************************************************#
				#*********************** end qg - channel ***********************#
				#****************************************************************#

			if qq_channel:
				#******************************************************************#
				#*********************** begin qq - channel ***********************#
				#******************************************************************#

				#------------------ Soft counterterm, qq - channel ------------------#

				# quark pdfs 

				ups = pdf_x1.up * pdf_x2.aup + pdf_x2.up * pdf_x1.aup
				downs = pdf_x1.d * pdf_x2.ad + pdf_x2.d * pdf_x1.ad
				stranges = pdf_x1.st * pdf_x2.ast + pdf_x2.st * pdf_x1.ast
				charms = pdf_x1.c * pdf_x2.ac + pdf_x2.c * pdf_x1.ac
				bottoms = pdf_x1.b * pdf_x2.ab + pdf_x2.b * pdf_x1.ab

				allpdf = ups + downs + stranges + charms + bottoms

				born_qq = GeV_pb * HypCubeFac * amp2_qaq * allpdf * PS  
											


				# Mismatch CounterTerm

				I_mismatch_qq_channel = coupl.alphas / np.pi * ( - T1_sq_qq - T2_sq_qq ) * L1cut / 2. * mismatch_integral[position_mm_and_soft]

				# Decoupled Soft CounterTerm 

				prefactor = coupl.alphas / np.pi * ( - T13_qq - T23_qq) * L1cut / 2

				I_dec_soft_qq_channel = prefactor * soft_integral[position_mm_and_soft]

				I_soft_qq = born_qq * ( I_mismatch_qq_channel + I_dec_soft_qq_channel )


				#------------------ initial state counterterm Pqq splitting, Born: q aq > h g, qq - channel ------------------#


				born =  GeV_pb * HypCubeFac * amp2_qaq * PS

				# quark pdfs for non convoluted piece

				pdf_aq_x2_q_x1 = pdf_x1.up * pdf_x2.aup + pdf_x1.d * pdf_x2.ad \
												+ pdf_x1.st * pdf_x2.ast + pdf_x1.c * pdf_x2.ac \
												+ pdf_x1.b * pdf_x2.ab

				pdf_aq_x1_q_x2 = pdf_x2.up * pdf_x1.aup + pdf_x2.d * pdf_x1.ad  \
											+ pdf_x2.st * pdf_x1.ast + pdf_x2.c * pdf_x1.ac \
											+ pdf_x2.b * pdf_x1.ab

				# quark pdfs for convoluted piece

				pdf_aq_convo_x2 = pdf_x1.up * pdf_x2_z2.aup + pdf_x1.d * pdf_x2_z2.ad \
											+ pdf_x1.st * pdf_x2_z2.ast + pdf_x1.c * pdf_x2_z2.ac \
											+ pdf_x1.b * pdf_x2_z2.ab

				pdf_aq_convo_x1 = pdf_x2.up * pdf_x1_z1.aup + pdf_x2.d * pdf_x1_z1.ad \
											+ pdf_x2.st * pdf_x1_z1.ast + pdf_x2.c * pdf_x1_z1.ac \
											+ pdf_x2.b * pdf_x1_z1.ab

				pdf_q_convo_x1 =  pdf_x1_z1.up * pdf_x2.aup + pdf_x1_z1.d * pdf_x2.ad \
												+ pdf_x1_z1.st * pdf_x2.ast + pdf_x1_z1.c * pdf_x2.ac \
												+ pdf_x1_z1.b * pdf_x2.ab

				pdf_q_convo_x2 = pdf_x2_z2.up * pdf_x1.aup + pdf_x2_z2.d * pdf_x1.ad \
													+ pdf_x2_z2.st * pdf_x1.ast + pdf_x2_z2.c * pdf_x1.ac\
													+ pdf_x2_z2.b * pdf_x1.ab


				# non convoluted piece

				I1 = coupl.alphas / np.pi * born * ( 0.5 * cf * (pdf_aq_x1_q_x2 + pdf_aq_x2_q_x1) * L2cut \
																																												- 1.5 * cf * (pdf_aq_x1_q_x2 + pdf_aq_x2_q_x1) * L1cut)


				# convoluted piece
				# Pqq splitting kernel in the as / pi convention

				Pqq_z1 = 0.5 * cf * (1. + z1new**2) / (1. - z1new)
				Pqq_z2 = 0.5 * cf * (1. + z2new**2) / (1. - z2new)

				# quark splitting
				qq_convo_x1 = (-np.log(x1) * z1new) * Pqq_z1 * ( pdf_q_convo_x1 / z1new  - pdf_aq_x2_q_x1) \
	

				qq_convo_x2 = (-np.log(x2) * z2new) * Pqq_z2 * ( pdf_q_convo_x2 / z2new  - pdf_aq_x1_q_x2) \
										+ 0.5 * cf * pdf_aq_x1_q_x2 * np.log(1. - x2)

				I_split_quark = coupl.alphas / np.pi * born * ( qq_convo_x1 + qq_convo_x2) * L1cut

				# anti-quark splitting

				aqaq_convo_x1 = (-np.log(x1) * z1new) * Pqq_z1 * ( pdf_aq_convo_x1 / z1new  - pdf_aq_x1_q_x2) \
													+ 0.5 * cf * pdf_aq_x1_q_x2 * np.log(1. - x1)

				aqaq_convo_x2 = (-np.log(x2) * z2new) * Pqq_z2 * ( pdf_aq_convo_x2 / z2new  - pdf_aq_x2_q_x1) \
													+ 0.5 * cf * pdf_aq_x2_q_x1 * np.log(1. - x2)


				I_split_aquark = coupl.alphas / np.pi * born * ( aqaq_convo_x1 + aqaq_convo_x2 ) * L1cut



				I_qq_splitting_qq_channel =  I1 + I_split_quark + I_split_aquark


				#------------------ initial state counterterm Pgq splitting, Born: q g  > h q, qq - channel ------------------#

				born = GeV_pb * HypCubeFac  * PS # wo amplitude squared

				# quark, a-quark pdf s

				q_pdf_x1 = ( pdf_x1.up + pdf_x1.d + pdf_x1.c + pdf_x1.st + pdf_x1.b )

				q_pdf_x2 = ( pdf_x2.up + pdf_x2.d + pdf_x2.c + pdf_x2.st + pdf_x2.b )

				aq_pdf_x1 = (pdf_x1.aup + pdf_x1.ad + pdf_x1.ac + pdf_x1.ast + pdf_x1.ab)

				aq_pdf_x2 = (pdf_x2.aup + pdf_x2.ad + pdf_x2.ac + pdf_x2.ast + pdf_x2.ab)

				# quark, a-quark pdfs for convolution

				q_convo_pdf_x1 = ( pdf_x1_z1.up + pdf_x1_z1.d + pdf_x1_z1.c \
													+ pdf_x1_z1.st + pdf_x1_z1.b )

				q_convo_pdf_x2 = ( pdf_x2_z2.up + pdf_x2_z2.d + pdf_x2_z2.c \
													+ pdf_x2_z2.st + pdf_x2_z2.b )

				aq_convo_pdf_x1 = (pdf_x1_z1.aup + pdf_x1_z1.ad + pdf_x1_z1.ac \
													+ pdf_x1_z1.ast + pdf_x1_z1.ab)

				aq_convo_pdf_x2 = (pdf_x2_z2.aup + pdf_x2_z2.ad + pdf_x2_z2.ac \
													+ pdf_x2_z2.ast + pdf_x2_z2.ab)    


				# Pgq splitting kernel, as / pi convention

				Pgq_z1 = 0.5 * cf * (1. + (1. - z1new)**2) / z1new
				Pgq_z2 = 0.5 * cf * (1. + (1. - z2new)**2) / z2new

				# quark - a-quark, quark splitting

				q_split_x1 = (-np.log(x1) * z1new) * 1. / z1new * Pgq_z1 * q_convo_pdf_x1 # u-channel

				q_split_x2 = (-np.log(x2) * z2new) * 1. / z2new * Pgq_z2 * q_convo_pdf_x2 # t-channel

				I_aqq_q_split = coupl.alphas / np.pi * born * ( aq_pdf_x2 * q_split_x1 * amp2_qg_u \
																								+ aq_pdf_x1 * q_split_x2 * amp2_qg_t) * L1cut

				# quark - a-quark, a-quark splitting

				aq_split_x1 = (-np.log(x1) * z1new) * 1. / z1new * Pgq_z1 * aq_convo_pdf_x1 # t-channel

				aq_split_x2 = (-np.log(x2) * z2new) * 1. / z2new * Pgq_z2 * aq_convo_pdf_x2 # u-channel

				I_aqq_aq_split = coupl.alphas / np.pi * born * ( q_pdf_x1 * aq_split_x2 * amp2_qg_u \
																+ q_pdf_x2 * aq_split_x1 * amp2_qg_t) * L1cut


				# quark - quark, quark splitting

				I_qq_q_split = coupl.alphas / np.pi * born * ( q_pdf_x1  * q_split_x2 * amp2_qg_u \
																										+ q_pdf_x2 * q_split_x1 * amp2_qg_t ) * L1cut

				# a-quark - a-quark, a-quark splitting


				I_aqaq_aq_split = coupl.alphas / np.pi * born * ( aq_pdf_x1  * aq_split_x2 * amp2_qg_u \
																												+ aq_pdf_x2 * aq_split_x1 * amp2_qg_t ) * L1cut




				I_gq_splitting_qq_channel = I_aqq_q_split + I_aqq_aq_split + I_qq_q_split + I_aqaq_aq_split


				Iqq_channel = I_soft_qq  + I_qq_splitting_qq_channel + I_gq_splitting_qq_channel

				counterterm += Iqq_channel
				#****************************************************************#
				#*********************** end qq - channel ***********************#
				#****************************************************************#

			if gg_channel:
				#******************************************************************#
				#*********************** begin gg - channel ***********************#
				#******************************************************************#


				#------------------ Soft counterterm, gg - channel ------------------#

				gluon_pdfs = pdf_x1.glu * pdf_x2.glu

				born =  GeV_pb * HypCubeFac * gluon_pdfs * amp2_gg * PS

				# Mismatch CounterTerm

				I_mismatch_gg_channel = coupl.alphas / np.pi * ( -2. * T2_gg ) * L1cut / 2. * mismatch_integral[position_mm_and_soft]

				# Decoupled Soft CounterTerm 

				prefactor = coupl.alphas / np.pi * ( - T13_gg - T23_gg) * L1cut / 2

				I_dec_soft_gg_channel = prefactor * soft_integral[position_mm_and_soft]

				I_soft_gg =  born * ( I_mismatch_gg_channel + I_dec_soft_gg_channel )


				#------------------ initial state counterterm Pgg splitting, Born: g g  > h g, gg - channel ------------------#

				conv1_HypCubeFac = -np.log(x1) * z1new
				conv2_HypCubeFac = -np.log(x2) * z2new


				born =  GeV_pb  * HypCubeFac * amp2_gg * PS

				# Convolutions

				# z1 convolution

				conv_x1_1  = (ca * (pdf_x1_z1.glu / z1new - pdf_x1.glu) / (1. - z1new) \
											+ ca * pdf_x1_z1.glu / z1new * ( (1. - 2. * z1new) / z1new + z1new * (1. - z1new)))

				conv_0_x1 = ca * pdf_x1.glu * np.log(1. - x1) 

				# z2 convolution

				conv_0_x2 = ca * pdf_x2.glu * np.log(1. - x2)

				conv_x2_1 = (ca * (pdf_x2_z2.glu / z2new - pdf_x2.glu) / (1. - z2new) \
										+ ca * pdf_x2_z2.glu / z2new * ((1. - 2. * z2new) / z2new + z2new * (1. - z2new)))


				# CounterTerm 


				I1 = coupl.alphas / np.pi * (0.5 * ag1 * gluon_pdfs * born ) * L2cut

				I2 = (coupl.alphas / np.pi ) * (bg1 * gluon_pdfs *  born ) * L1cut 

				I3 =  coupl.alphas / (np.pi) * born * ( pdf_x2.glu * ( conv_0_x1 \
							+ conv_x1_1 * conv1_HypCubeFac  - bg1 / 2. * pdf_x1.glu) + pdf_x1.glu * (conv_0_x2 \
							+ conv2_HypCubeFac * conv_x2_1 - bg1 / 2. * pdf_x2.glu)) * L1cut

				I_gg_splitting_gg_channel = I1 + I2 + I3


				#------------------ initial state counterterm Pqg splitting, Born: q g  > h q, gg - channel ------------------#

				# Amplitude Squared for Born: q g > q h, see Ravindran 2002

				born =  GeV_pb  * HypCubeFac * PS

				# Pqg splitting  kernels in as / pi convention

				splitting_kernel_qg_1 = 0.5 * tr * (z1new**2 + (1 - z1new)**2)
				splitting_kernel_qg_2 = 0.5 * tr * (z2new**2 + (1 - z2new)**2)

				conv_x1_1 = 1 / z1new * splitting_kernel_qg_1 * pdf_x1_z1.glu

				conv_x2_1 = 1 / z2new * splitting_kernel_qg_2 * pdf_x2_z2.glu



				I_qg_splitting_gg_channel = coupl.alphas / (np.pi) * ( born ) * (amp2_qg_u * pdf_x2.glu * conv_x1_1 * conv1_HypCubeFac \
																	+ amp2_qg_t * pdf_x1.glu * conv_x2_1 * conv2_HypCubeFac) * L1cut * 2. * nf


				Igg_channel = I_soft_gg + I_gg_splitting_gg_channel + I_qg_splitting_gg_channel


				counterterm += Igg_channel
				#****************************************************************#
				#*********************** end gg - channel ***********************#
				#****************************************************************#

			#Hjet_ct_NLO = Igg_channel + Iqq_channel + Iqg_channel

			if writehisto:

					if iter[0] > 2 :
						iterations.append(iter[0])
						wgt_vegas.append(weight[0])
						integrand_wgt.append(counterterm)


						#--- 2 -> 2 Kin., p(P1) p(P2) -> j(p3) + H(p4)

						E3 = np.sqrt(Q2) / 2. * (1. - mH**2 / Q2)
						E4 = np.sqrt(Q2) / 2. * (1. + mH**2 / Q2)

						#--- momenta in partonic frame ---#

						p1 = np.sqrt(Q2) / 2. * np.array([1,0,0,1])
						p2 = np.sqrt(Q2) / 2. * np.array([1,0,0,-1])
						b = 1. 
						if np.random.rand() < 0.5:
								b = -1.

						p3 = E3 * np.array([1,  b * np.sqrt(- 2. * t / (np.sqrt(Q2) * E3) * (1. + 0.5 * t / (np.sqrt(Q2) * E3))) * np.cos(phinew), \
											b * np.sqrt(- 2. * t / (np.sqrt(Q2) * E3) * (1. + 0.5 * t / (np.sqrt(Q2) * E3))) * np.sin(phinew) , \
											1. + t / (np.sqrt(Q2) * E3)])

						p4 = p1 + p2 - p3

						#--- momenta in hadronic frame ---#
						refvec = np.sqrt(S) / 2. * np.array([x1 + x2,0,0,x2 - x1])

						p1had = longi_boost(p1,refvec)
						p2had = longi_boost(p2,refvec)
						p3had = longi_boost(p3,refvec)
						p4had = p1had + p2had - p3had

						#-- Observables --#

						yH  = 0.5 * np.log((p4had[0] + p4had[3]) / (p4had[0] - p4had[3]))
						yJet = 0.5 * np.log((p3had[0] + p3had[3]) / (p3had[0] - p3had[3]))

						ptH = np.sqrt(p4had[1]**2 + p4had[2]**2)
						ptJet = np.sqrt(p3had[1]**2 + p3had[2]**2)
						qt = np.sqrt((p4had[1]+p3had[1])**2 + (p4had[2]+p3had[2])**2)

						mH_jet = np.sqrt((p4had[0] + p3had[0])**2 - (p4had[1] + p3had[1])**2 - (p4had[2] + p3had[2])**2 - (p4had[3] + p3had[3])**2)
						# #--- save observables values for distributions ---#

						rap_higgs.append(yH)

						pt_Higgs.append(ptH)

						pt_jet.append(ptJet)

						m_higgs_jet.append(mH_jet)

						pt_higgs_jet.append(qt)
								





			ff[0] = counterterm
			return 0


		NDIM = 6  
		EPSREL = ep_rel
		NINCREASE = N_increase
		MAXEVAL = max_eval
				
		def print_header(name):
					print('-------------------- %s test -------------------' % name)
		def print_results(name, results):
			keys = ['nregions', 'neval', 'fail']
			text = ["%s %d" % (k, results[k]) for k in keys if k in results]
			print("%s RESULt:\t" % name.upper() + "\t".join(text))
			for comp in results['results']:
				print("%s RESULT:\t" % name.upper() + \
					"%(integral).8f +- %(error).8f\tp = %(prob).3f\n" % comp)

		from os import environ as env
		verbose = 2
		if 'CUBAVERBOSE' in env:
			verbose = int(env['CUBAVERBOSE'])

		print_header('Vegas')
		print_results('Vegas', pycuba.Vegas(Integrand, NDIM,epsrel=EPSREL,maxeval=MAXEVAL,\
						nincrease=NINCREASE,verbose=2))

				#pycuba.Vegas(Integrand, NDIM, verbose=2)


	if writehisto:
		wgts = np.array(wgt_vegas) * np.array(integrand_wgt) / (iterations[-1] - 2)

		rapH = myHisto("Higgs Rapidity")
		rapH.fill(rap_higgs,-5.,5.,0.4,wgts / 0.4)
		np.savez("raphiggs_ct",hist = rapH.histo ,binedges = rapH.binedges ,hist_err = rapH.error)

		pth = myHisto("Higgs pt")
		pth.fill(pt_Higgs,0.,1200.,15,wgts/15.)
		np.savez("pthiggs_ct",hist = pth.histo ,binedges = pth.binedges ,hist_err = pth.error)

		ptj = myHisto('jet pt')
		ptj.fill(pt_jet,0.,1200.,15.,wgts/15.)
		np.savez("ptjet_ct",hist = ptj.histo ,binedges = ptj.binedges ,hist_err = ptj.error)


		mHj = myHisto('Higgs jet invariant mass')
		mHj.fill(m_higgs_jet,0.,1200.,15.,wgts/15.)
		np.savez("mass_higgs_jet_ct",hist = mHj.histo ,binedges = mHj.binedges ,hist_err = mHj.error)

		qt_hj = myHisto('qt imbalance')
		qt_hj.fill(pt_higgs_jet,0.,1200.,15.,wgts/15.)
		np.savez("qt_imbalance_ct",hist = qt_hj.histo ,binedges = qt_hj.binedges ,hist_err = qt_hj.error)		








