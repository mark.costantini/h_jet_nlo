

if __name__ == "__main__":
	import numpy as np 
	import matplotlib.pyplot as plt
	from scipy import optimize
	from plotter import plot_qtsub, plot_qtsub_LR, plot_hist, plot_nlo
	import sys


	#----- Bool values ------#

	load_qt_data = True 
	plotFullR = True
	plotLR = False

	plothisto = False 

	load_pp_data = False 
	plotNLO = False 

	#------ Load Data from qt_sub_files -------#
	if load_qt_data:
		
		#--- R = 0.1 ---#
		#--- gg only
		
		ct_1, ct_1_err = np.loadtxt("files/qt_sub_files/ct_hjet_ggonly_125.1_125.1_0.1_13TeV.txt",unpack = True)
		ct_1_lR, ct_1_lR_err = np.loadtxt("files/qt_sub_files/ct_hjet_ggonly_125.1_125.1_0.1_13TeV_leadingR.txt",unpack = True)
		qt_1,re_1, re_1_err = np.loadtxt("files/qt_sub_files/mcfm_real_ggonly_125.1_125.1_0.1_30d0_ult_prec.txt",unpack = True)

		#--- R = 0.3 ---#
		ct_3, ct_3_err = np.loadtxt("files/qt_sub_files/ct_hjet_ggonly_125.1_125.1_0.3_13TeV.txt",unpack = True)
		ct_3_lR, ct_3_lR_err = np.loadtxt("files/qt_sub_files/ct_hjet_ggonly_125.1_125.1_0.3_13TeV_leadingR.txt",unpack = True)
		qt_3,re_3, re_3_err = np.loadtxt("files/qt_sub_files/mcfm_real_ggonly_125.1_125.1_0.3_30d0_ult_prec.txt",unpack = True)


		#--- R = 0.8 ---#
		ct_8, ct_8_err = np.loadtxt("files/qt_sub_files/ct_hjet_ggonly_125.1_125.1_0.8_13TeV.txt",unpack = True)
		ct_8_lR, ct_8_lR_err = np.loadtxt("files/qt_sub_files/ct_hjet_ggonly_125.1_125.1_0.8_13TeV_leadingR.txt",unpack = True)
		qt_8,re_8, re_8_err = np.loadtxt("files/qt_sub_files/mcfm_real_ggonly_125.1_125.1_0.8_30d0_ult_prec.txt",unpack = True)


		#--- noglue
		ct_qq_1, ct_qq_1_err = np.loadtxt("files/qt_sub_files/ct_hjet_qqonly_125.1_125.1_0.1_13TeV.txt",unpack = True)
		qt_qq_1,re_qq_1, re_qq_1_err = np.loadtxt("files/qt_sub_files/mcfm_real_noglue_125.1_125.1_0.1_30d0_ult_prec.txt",unpack = True)

		#--- qgonly
		ct_qg_1, ct_qg_1_err = np.loadtxt("files/qt_sub_files/ct_hjet_qgonly_125.1_125.1_0.1_13TeV.txt",unpack = True)
		qt_qg_1,re_qg_1, re_qg_1_err = np.loadtxt("files/qt_sub_files/mcfm_real_qgonly_125.1_125.1_0.1_30d0_ult_prec.txt",unpack = True)

		

	if load_pp_data:

		ct_11, ct_11_err = np.loadtxt("files/full_nlo_files/ct_pphjet_125.1_125.1_0.1_13TeV.txt",unpack = True)
		qt_11,re_11, re_11_err = np.loadtxt("files/full_nlo_files/mcfm_real_pp_125.1_125.1_0.1_30d0_13TeV.txt",unpack = True)
		hf_11, hf_11_err = np.loadtxt("files/full_nlo_files/hfunction_pphjet_125.1_125.1_0.1_13TeV.txt",unpack = True)
		nlo_11 = 13.2496
		nlo_11_err = 0.72590E-02

		ct_20, ct_20_err = np.loadtxt("files/full_nlo_files/ct_pphjet_250.2_62.55_0.1_13TeV.txt",unpack = True)
		qt_20,re_20, re_20_err = np.loadtxt("files/full_nlo_files/mcfm_real_pp_250.2_62.55_0.1_30d0_13TeV.txt",unpack = True)		
		hf_20, hf_20_err = np.loadtxt("files/full_nlo_files/hfunction_pphjet_250.2_62.55_0.1_13TeV.txt",unpack = True)
		nlo_20 = 15.7008
		nlo_20_err =  0.96920E-02



		ct_02, ct_02_err = np.loadtxt("files/full_nlo_files/ct_pphjet_62.55_250.2_0.1_13TeV.txt",unpack = True)
		qt_02,re_02, re_02_err = np.loadtxt("files/full_nlo_files/mcfm_real_pp_62.55_250.2_0.1_30d0_13TeV.txt",unpack = True)		
		hf_02, hf_02_err = np.loadtxt("files/full_nlo_files/hfunction_pphjet_62.55_250.2_0.1_13TeV.txt",unpack = True)
		nlo_02 = 11.1381
		nlo_02_err = 0.57805E-02
	#----- plot stuff ----#

	if plotFullR:
		# plot_qtsub(qt_1,ct_1,ct_1_err,re_1,re_1_err)

		# plot_qtsub(qt_3,ct_3,ct_3_err,re_3,re_3_err)

		# plot_qtsub(qt_8,ct_8,ct_8_err,re_8,re_8_err)
		
		# plot_qtsub(qt_qq_1,ct_qq_1,ct_qq_1_err,re_qq_1,re_qq_1_err)

		plot_qtsub(qt_qg_1,ct_qg_1,ct_qg_1_err,re_qg_1,re_qg_1_err)

	


	if plotLR:

		# plot_qtsub_LR(qt_1,ct_1,ct_1_err,re_1,re_1_err,ct_1_lR,ct_1_lR_err)

		# plot_qtsub_LR(qt_3,ct_3,ct_3_err,re_3,re_3_err,ct_3_lR,ct_3_lR_err)

		plot_qtsub_LR(qt_8,ct_8,ct_8_err,re_8,re_8_err,ct_8_lR,ct_8_lR_err)



	if plothisto:
		
		# plot_hist("files/histo_files/raphiggs_ct.npz","files/histo_files/raphiggs_hf.npz",\
		# 	"files/histo_files/ggfus1_nlo_NNPDF31_125__125__125_13TeV_rap_higgs.txt",\
		# 	"files/histo_files/ggfus1_real_NNPDF31_125__125__125_13TeV_rap_higgs.txt"\
		# 	,"files/histo_files/ggfus1_lo_NNPDF31_125__125__125_13TeV_rap_higgs.txt"\
		# 	,r"$\frac{d\sigma}{dy_H}(pb)$",r"$\mu_R=\mu_F=m_H,13TeV$",r"$y_H$",\
		# 		r"$R$")

		# plot_hist("files/histo_files/mass_higgs_jet_ct.npz","files/histo_files/mass_higgs_jet_hf.npz",\
		# 	"files/histo_files/ggfus1_nlo_NNPDF31_125__125__125_13TeV_mass_higgs_jet.txt",\
		# 	"files/histo_files/ggfus1_real_NNPDF31_125__125__125_13TeV_mass_higgs_jet.txt",\
		# 	"files/histo_files/ggfus1_lo_NNPDF31_125__125__125_13TeV_mass_higgs_jet.txt"\
		# 	,r"$d\sigma/dm_{Hj} (pb/GeV)$",r"$\mu_R=\mu_F=m_H,13TeV$",r"$m_{Hj}$ (GeV)"\
		# 	, r"$R$")

		# plot_hist("files/histo_files/tmp/qt_imbalance_ct.npz","files/histo_files/tmp/qt_imbalance_ct.npz",\
		# 	"files/histo_files/ggfus1_nlo_NNPDF31_125__125__125_13TeV_qt.txt",\
		# 	"files/histo_files/ggfus1_real_NNPDF31_125__125__125_13TeV_qt.txt"\
		# 	,r"$\frac{d\sigma}{dq_T} (pb/GeV)$",r"$\mu_R=\mu_F=m_H,13TeV$",r"$q_T$ (GeV)"\
		# 	, r"$\frac{d\sigma}{dq_T} - \frac{d\sigma^{mcfm}}{dq_T$")

		plot_hist("files/histo_files/pthiggs_ct.npz","files/histo_files/pthiggs_hf.npz",\
			"files/histo_files/ggfus1_nlo_NNPDF31_125__125__125_13TeV_pthiggs.txt",\
			"files/histo_files/ggfus1_real_NNPDF31_125__125__125_13TeV_pthiggs.txt",\
			"files/histo_files/ggfus1_lo_NNPDF31_125__125__125_13TeV_pthiggs.txt"\
			,r"$d\sigma/dp_T^{H} (pb/GeV)$",r"$\mu_R=\mu_F=m_H,13TeV$",r"$p_T^{Higgs}$ (GeV)"\
			,r"$R$")

		plot_hist("files/histo_files/ptjet_ct.npz","files/histo_files/ptjet_hf.npz",\
			"files/histo_files/ggfus1_nlo_NNPDF31_125__125__125_13TeV_ptjet.txt",\
			"files/histo_files/ggfus1_real_NNPDF31_125__125__125_13TeV_ptjet.txt",\
			"files/histo_files/ggfus1_lo_NNPDF31_125__125__125_13TeV_ptjet.txt"\
			,r"$d\sigma/dp_T^j (pb/GeV)$",r"$\mu_R=\mu_F=m_H,13TeV$",r"$p_T^{jet} (GeV)$"\
			,r"$R$")

		



	if plotNLO:

		 
		plot_nlo(qt_11,ct_11,ct_11_err,hf_11,hf_11_err,re_11,re_11_err,nlo_11,nlo_11_err)

		plot_nlo(qt_20,ct_20,ct_20_err,hf_20,hf_20_err,re_20,re_20_err,nlo_20,nlo_20_err)

		plot_nlo(qt_02,ct_02,ct_02_err,hf_02,hf_02_err,re_02,re_02_err,nlo_02,nlo_02_err)
		


