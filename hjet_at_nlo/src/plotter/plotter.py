import numpy as np 
import matplotlib.pyplot as plt
from scipy import optimize


def qt_sub_fit(rcut,A,B,C):
	return A + B * rcut  + C * rcut**2 


def plot_qtsub(rcut,ct,ct_err,real,real_err,extrapolation=True):

	sub = (real - ct)
	sub_err = np.sqrt(real_err**2 + ct_err**2)
	if extrapolation:

		A,B,C = optimize.curve_fit(qt_sub_fit,rcut,sub)[0]
		A_err,B_err,C_err = np.sqrt(np.diag(optimize.curve_fit(qt_sub_fit,rcut,sub)[1]))

		extr = A
		extr_err = A_err
		print(extr_err)
		plt.errorbar(rcut,sub / extr -1 ,np.sqrt((1./extr * sub_err)**2 + (sub/extr**2 * extr_err)**2 ),fmt = 'o',ecolor = "g",color = "b",label = "$\mathtt{hjet\_at\_nlo}$, full R")
		plt.plot(rcut,np.zeros(len(rcut)),color = "c",alpha = 1)
		plt.fill_between(rcut,0. + extr_err,0. - extr_err,color = 'red',alpha = 0.4,label = "extr, full R")
		plt.grid()
		plt.title(r"$\frac{\sigma_{2-jet}^{gg} - \sigma_{CT}^{gg}}{\sigma_{extrapolated}} - 1$",loc = "left")
		plt.title(r"$qg$-channel")
		plt.title(r"$\mu_F = \mu_R = m_H, 13 TeV$",loc = "right")
		plt.text(0.0001,-0.022,"Jet Radius = 0.1", bbox=dict(facecolor='red', alpha=0.5))
		plt.text(0.0001,-0.025,"ptHiggs min = 30 GeV", bbox=dict(facecolor='green', alpha=0.5))
		plt.xlabel("$r_{cut}$")
		plt.legend()
		plt.show()
		

	else:
		plt.errorbar(rcut,sub,sub_err,fmt = 'o',ecolor = "g",color = "b",label = "$\mathtt{hjet\_at\_nlo}$")
		
		plt.grid()
		plt.title(r"$\sigma_{2-jet}^{gg} - \sigma_{CT}^{gg}$",loc = "left")
		plt.title(r"$gg \rightarrow H j$")
		plt.title(r"$\mu_F = \mu_R = m_H, 13 TeV$",loc = "right")
		plt.xlabel("$r_{cut}$")
		plt.legend()
		plt.show()


def plot_qtsub_LR(rcut,ct,ct_err,real,real_err,ct_LR,ct_LR_err,extrapolation=True):

	sub = (real - ct)
	sub_err = np.sqrt(real_err**2 + ct_err**2)

	sub_LR = (real - ct_LR)
	sub_LR_err = np.sqrt(real_err**2 + ct_LR_err**2)

	if extrapolation:
		
		popt,pcov = optimize.curve_fit(qt_sub_fit,rcut,sub)
		perr = np.sqrt(np.diag(pcov))

		r = np.array([1,rcut,np.log(rcut)* rcut])
		fit_err = np.sqrt(sum(r*perr))

		extr = popt[0]
		extr_err = perr[0]

		plt.errorbar(rcut,sub_LR / extr -1 ,np.sqrt((1./extr * sub_LR_err)**2 + (sub_LR/extr**2 * extr_err)**2 ),fmt = 'o',ecolor = "g",color = "b",label = "$\mathtt{hjet\_at\_nlo}$, leading R")
		# plt.plot(rcut,np.zeros(len(rcut)),color = "c",alpha = 1)
		plt.fill_between(rcut,0. + extr_err,0. - extr_err,color = 'red',alpha = 0.4,label = "extr, full R")
		plt.grid()
		plt.title(r"$\frac{\sigma_{2-jet}^{gg} - \sigma_{CT}^{gg}}{\sigma_{extrapolated}} - 1$",loc = "left")
		plt.title(r"$gg$-channel")
		plt.title(r"$\mu_F = \mu_R = m_H, 13 TeV$",loc = "right")
		plt.text(0.0001,-0.53,"Jet Radius = 0.8", bbox=dict(facecolor='red', alpha=0.5))
		plt.text(0.0001,-0.7,"pt Jet min = 30 GeV", bbox=dict(facecolor='green', alpha=0.5))
		plt.xlabel("$r_{cut}$")
		plt.legend()
		plt.show()
		

	else:
		plt.errorbar(rcut,sub_LR,sub_LR_err,fmt = 'o',ecolor = "g",color = "b",label = "$\mathtt{hjet\_at\_nlo}$")
		
		plt.grid()
		# plt.title(r"$\sigma_{2-jet}^{gg} - \sigma_{CT}^{gg}$",loc = "left")
		# plt.title(r"$gg \rightarrow H j$")
		# plt.title(r"$\mu_F = \mu_R = m_H, 13 TeV$",loc = "right")
		plt.xlabel("$r_{cut}$")
		plt.legend()
		plt.show()



def plot_hist(ct_npz,hf_npz,mcfm_file,mcfm_real_file,mcfm_lo,title_left,title_right,xlabel,ylabel):

	#---- load files from hjet_at_nlo ----#
	c = np.load(ct_npz)
	h = np.load(hf_npz)

	#---- load files from mcfm ----#
	y_mcfm,h_mcfm,h_mcfm_err = np.loadtxt(mcfm_file,unpack = True)
	y_r,real_mcfm,real_mcfm_err = np.loadtxt(mcfm_real_file,unpack = True)
	y_lo,h_lo_mcfm,h_lo_mcfm_err = np.loadtxt(mcfm_lo,unpack = True)

	histo = h['hist'] + (real_mcfm*1e-3 - c['hist'])
	
	histo_err = np.sqrt(h["hist_err"]**2 + (real_mcfm_err*1e-3)**2 + c['hist_err']**2 )

	

	plt.errorbar(y_mcfm,histo,histo_err,drawstyle = 'steps-mid',color = 'r',ecolor = 'g',label = "$q_T - subtraction, r_{cut} = 0.0003$")
	plt.errorbar(y_mcfm,h_mcfm*1e-3,h_mcfm_err*1e-3,drawstyle = 'steps-mid',label = "$\mathtt{mcfm}$",color = "c",ecolor = "y")
	plt.grid(linestyle = ':',linewidth = 0.8)
	plt.title(r"$\frac{d\sigma}{dy_H} (pb)$",loc = "left")
	plt.title("$\mu_R = \mu_F = m_H$, 13 TeV",loc = "right")
	plt.xlabel(xlabel)
	plt.legend()
	
	plt.close()
	plt.show()

	fig, (ax0, ax1) = plt.subplots(2, sharex=True,gridspec_kw={'height_ratios': [4.5, 1]})

	ax0.errorbar(y_mcfm,histo, yerr=histo_err, drawstyle='steps-mid',ecolor = "g",color = "r",label = "$q_T - subtraction, r_{cut} = 0.0003$")
	ax0.errorbar(y_mcfm,h_mcfm*1e-3,yerr = h_mcfm_err*1e-3,drawstyle = 'steps-mid',color = "c",ecolor = "y",label = "$\mathtt{mcfm}$")
	ax0.errorbar(y_lo,h_lo_mcfm*1e-3,yerr = h_lo_mcfm_err*1e-3,drawstyle = 'steps-mid',color = "orange",ecolor = "b",label = "LO")
	ax0.set_yscale("log")
	ax0.legend()
	ax0.set_title(title_left,loc = "left")
	ax0.set_title(title_right,loc = "right")
	ax0.grid()

	ax1.plot(y_mcfm,np.ones(len(y_mcfm)),color = "b")
	#ax1.plot(bins,max([y,h_mcfm*1e-3*0.4])/(min([y,h_mcfm*1e-3*0.4])),drawstyle="steps-mid",color = 'r')
	R = (histo/(h_mcfm*1e-3))
	ax1.plot(y_mcfm,R,drawstyle="steps-mid",color = 'r')
	ax1.set_ylim(0.95,1.05)
	# ax1.set_yticks(np.arange(min(R), max(R), 0.001))
	ax1.set_xlim(0,320)
	ax1.set_ylabel(ylabel)
	ax1.set_xlabel(xlabel)
	
	ax1.grid()
	plt.show()



def plot_nlo(rcut,ct,ct_err,hf,hf_err,real,real_err,mcfm_nlo,mcfm_nlo_err,normalized=True):

	sub = (real - ct)
	sub_err = np.sqrt(real_err**2 + ct_err**2)
	nlo = hf + sub
	nlo_err = np.sqrt(hf_err**2 + sub_err**2)

	popt,pcov = optimize.curve_fit(qt_sub_fit,rcut,nlo,sigma=nlo_err)
	perr = np.sqrt(np.diag(pcov))
	print(popt[0],perr[0])

	if normalized:

		norm = nlo / mcfm_nlo - 1
		norm_err = np.sqrt((1./mcfm_nlo * nlo_err)**2 + (nlo/mcfm_nlo**2 * mcfm_nlo_err)**2)
		
		plt.errorbar(rcut,norm,norm_err,fmt = 'o',ecolor = "g",color = "b",label = "$\mathtt{hjet\_at\_nlo}$")
		h = 0.0003
		x = np.zeros(len(rcut))
		plt.scatter(rcut,x,color = "red",label = "cs")
		# plt.fill_between(rcut,x+h,x-h,color = "red",label = "cs")
		plt.grid()
		plt.title(r"$\frac{\sigma_{NLO}^{hjet}}{\sigma_{NLO}^{cs}} - 1$",loc = "left")
		plt.title(r"$pp \to H j$")
		plt.title(r"$\mu_F = \mu_R= m_H, 13 TeV$",loc = "right")
		plt.text(0.0001,0.021,"Jet Radius = 0.1", bbox=dict(facecolor='red', alpha=0.5))
		plt.text(0.0001,0.018,"ptHiggs min = 30 GeV", bbox=dict(facecolor='green', alpha=0.5))
		plt.xlabel("$r_{cut}$")

		plt.legend()
		plt.show()
		

	else:
		plt.errorbar(rcut,nlo,nlo_err,fmt = 'o',ecolor = "g",color = "b",label = "$\mathtt{hjet\_at\_nlo}$")
		
		h = np.ones(len(rcut)) *mcfm_nlo_err
		x = np.ones(len(rcut)) * mcfm_nlo
		plt.fill_between(rcut,x+h,x-h,color = 'r',alpha = 0.4,label = "cs")
		plt.grid()
		plt.title(r"$\frac{\sigma_{NLO}^{hjet}}{\sigma_{NLO}^{cs}} - 1$",loc = "left")
		plt.title(r"$pp \to H j$")
		plt.title(r"$\mu_F = \mu_R = m_H, 13 TeV$",loc = "right")
		plt.xlabel("$r_{cut}$")
		plt.legend()
		plt.show()






